import { Injectable } from '@nestjs/common';
import { InjectModel, Model } from 'nestjs-dynamoose';
import * as uuid from 'uuid';
import { CreateSysParamInput } from '../input/create-sys-param.input';
import { SysParam, SysParamKey } from '../model/sys-param.model';

@Injectable()
export class SysParamService {
  constructor(
    @InjectModel('SysParam')
    private model: Model<SysParam, SysParamKey>,
  ) {}

  create(input: CreateSysParamInput) {
    return this.model.create({
      ...input,
      id: uuid.v4(),
      active: input.active ?? true,
      version: 1,
      createDate: new Date().toISOString(),
      updateDate: new Date().toISOString(),
    });
  }

  findAll(applicationId: string) {
    const query = this.model.query().where('applicationId').eq(applicationId);
    query.filter('active').eq(true);
    return query.exec();
  }

  find(applicationId: string, subApplicationId?: string) {
    const query = this.model.query().where('applicationId').eq(applicationId);
    query.where('subApplicationId').eq(subApplicationId);
    query.filter('active').eq(true);
    return query.exec();
  }
}
