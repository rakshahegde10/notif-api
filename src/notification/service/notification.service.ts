import {
  BadRequestException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import moment from 'moment';
import { InjectModel, Model } from 'nestjs-dynamoose';
import * as uuid from 'uuid';
import { logger } from '../../util/logger.util';
import { CreateNotificationInput } from '../input/create-notification.input';
import { RetrieveNotificationInput } from '../input/retrieve-notification.input';
import { UpdateNotificationInput } from '../input/update-notification.input';
import { NotificationStatus } from '../model/notification.enum';
import { Notification, NotificationKey } from '../model/notification.model';
import {
  buildIndexKeys,
  extractLoginRequired,
} from '../util/notification.util';

@Injectable()
export class NotificationService {
  constructor(
    @InjectModel('Notification')
    private model: Model<Notification, NotificationKey>,
  ) {}

  create(input: CreateNotificationInput) {
    const retentionDay = parseInt(
      process.env.NOTIF_REC_RETENTION_DAY || '31',
      10,
    );
    const createDate = input.createDate ?? new Date().toISOString();
    const ttl =
      Math.floor(new Date(createDate).getTime() / 1000) +
      retentionDay * 24 * 3600;

    const loginRequired = extractLoginRequired(input.data);
    return this.model.create({
      ...input,
      ...buildIndexKeys(input),
      id: uuid.v4(),
      version: 2,
      customSearchKey: JSON.stringify(
        loginRequired !== undefined
          ? { ['login_required']: loginRequired ? 'Y' : 'N' }
          : {},
      ),
      createDate: createDate,
      period: moment.utc(createDate).format('YYYY-MM-DD'),
      ttl: ttl,
      status: input.status ?? NotificationStatus.New,
    });
  }

  find(
    input: RetrieveNotificationInput & {
      requestDate: string;
    },
    retention: number,
  ) {
    const { appTargetId, appUserTargetId, appUserId } = buildIndexKeys(input);
    const fromDate = moment(input.requestDate)
      .subtract(retention, 'days')
      .toISOString();
    let index;
    let indexString;

    logger.info(
      null,
      'appTargetId:',
      appTargetId,
      ' appUserTargetId:',
      appUserTargetId,
      ' appUserId:',
      appUserId,
    );

    if (
      (input.targetId && input.userId) ||
      (!input.targetId && !input.userId)
    ) {
      // with target id and user id
      logger.info(null, 'target id and user id');
      indexString = 'appUserTargetId';
      index = appUserTargetId;
    } else if (input.userId && !input.targetId) {
      // with user id and no target id
      logger.info(null, 'user id only');
      indexString = 'appUserId';
      index = appUserId;
    } else {
      // with target id only
      logger.info(null, 'target id only');
      indexString = 'appTargetId';
      index = appTargetId;
    }

    logger.info(
      null,
      'indexString:',
      indexString,
      ' index:',
      index,
      'fromDate: ',
      fromDate,
    );

    let query = this.model
      .query(indexString)
      .eq(index)
      .where('createDate')
      .ge(fromDate)
      .where('status')
      .not()
      .eq(NotificationStatus.Delete);
    if (input.subApplicationId && indexString === 'appTargetId') {
      query = query.filter('subApplicationId').eq(input.subApplicationId);
    }

    logger.debug(null, 'query: ', JSON.stringify(query));

    return query.exec();
  }

  async update(id: string, userid: string, input: UpdateNotificationInput) {
    const items = await this.model.query('id').eq(id).exec();
    if (items.length === 1) {
      /* istanbul ignore next */
      if (process.env.NODE_ENV !== 'test' && items[0].userId !== userid) {
        throw new UnauthorizedException('Invalid userid');
      }
      const { appTargetId, createDate } = items[0];
      return await this.model.update({ appTargetId, createDate }, input);
    } else {
      throw new BadRequestException();
    }
  }
}
