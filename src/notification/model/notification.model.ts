import { Field, ID, ObjectType } from '@hardyscc/nestjs-graphql';
import { NotificationStatus } from './notification.enum';

export type NotificationKey = {
  appTargetId: string;
  createDate: string;
};

@ObjectType()
export class Notification {
  @Field(/* istanbul ignore next */ () => ID)
  id: string;

  @Field()
  applicationId: string;

  @Field({ nullable: true })
  subApplicationId?: string;

  @Field({ nullable: true })
  targetId?: string;

  @Field({ nullable: true })
  userId?: string;

  @Field()
  alert: string;

  @Field({ nullable: true })
  badge?: number;

  @Field({ nullable: true })
  sound?: string;

  @Field({ nullable: true })
  data?: string;

  @Field({ nullable: true })
  createDate: string;

  @Field({ nullable: true })
  period: string;

  // TODO: remove optional later
  @Field(/* istanbul ignore next */ () => NotificationStatus, {
    nullable: true,
  })
  status?: NotificationStatus;

  appTargetId: string;

  appUserTargetId: string;

  appUserId: string;

  customSearchKey: string;

  version: number;

  ttl: number;

  //transient props
  loginRequired?: boolean;
}
