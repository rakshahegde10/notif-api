import { registerEnumType } from '@hardyscc/nestjs-graphql';

enum NotificationStatus {
  New = 'New',
  Read = 'Read',
  Delete = 'Delete',
}

registerEnumType(NotificationStatus, {
  name: 'NotificationStatus',
});

export { NotificationStatus };
