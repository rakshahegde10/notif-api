import { Field, ID, ObjectType } from '@hardyscc/nestjs-graphql';

export type SysParamKey = {
  id: string;
};

@ObjectType()
export class SysParam {
  @Field(/* istanbul ignore next */ () => ID)
  id: string;

  @Field()
  applicationId: string;

  @Field()
  subApplicationId: string;

  // TODO: remove later
  @Field()
  parameters?: string;

  @Field({ nullable: true })
  retention?: number;

  @Field({ nullable: true })
  allowGuest?: boolean;

  @Field({ nullable: true })
  allowSendGeneral?: boolean;

  @Field({ nullable: true })
  allowSendPersonal?: boolean;

  @Field({ nullable: true })
  allowSendTarget?: boolean;

  @Field({ nullable: true })
  active: boolean;

  createDate: string;

  updateDate: string;

  version: number;
}
