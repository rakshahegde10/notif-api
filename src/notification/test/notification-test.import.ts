import { ConfigModule } from '@nestjs/config';
import { DynamooseModule } from 'nestjs-dynamoose';
import { NotificationSchema } from '../schema/notification.schema';
import { SysParamSchema } from '../schema/sys-param.schema';

export const NotificationTestImports = [
  ConfigModule.forRoot(),
  DynamooseModule.forRoot({
    local: 'http://localhost:8001',
    aws: { region: 'local' },
    model: {
      create: false,
      prefix: `${process.env.SERVICE}-${process.env.STAGE}-`,
    },
  }),
  DynamooseModule.forFeature([
    {
      name: 'SysParam',
      schema: SysParamSchema,
    },
    {
      name: 'Notification',
      schema: NotificationSchema,
    },
  ]),
];
