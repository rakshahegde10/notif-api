import { Field, InputType } from '@hardyscc/nestjs-graphql';
import {
  IsBoolean,
  IsDateString,
  IsInt,
  IsNotEmpty,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';
import { NotificationStatus } from '../model/notification.enum';

@InputType()
export class CreateNotificationInput {
  @IsNotEmpty()
  @IsString()
  @Field()
  applicationId: string;

  @IsOptional()
  @IsString()
  @Field({ nullable: true })
  subApplicationId?: string;

  @IsOptional()
  @IsString()
  @Field({ nullable: true })
  targetId?: string;

  @IsOptional()
  @IsString()
  @Field({ nullable: true })
  userId?: string;

  @IsNotEmpty()
  @IsString()
  @Field()
  alert: string;

  @IsOptional()
  @IsInt()
  @Min(0)
  @Field({ nullable: true })
  badge?: number;

  @IsOptional()
  @IsString()
  @Field({ nullable: true })
  sound?: string;

  @IsOptional()
  @IsString()
  @Field({ nullable: true })
  data?: string;

  @IsOptional()
  @IsDateString()
  @Field({ nullable: true })
  createDate?: string;

  @IsOptional()
  @IsString()
  @Field({ nullable: true })
  status?: NotificationStatus;

  @IsOptional()
  @IsBoolean()
  @Field({ nullable: true })
  loginRequired?: boolean;
}
