import { Field, InputType } from '@hardyscc/nestjs-graphql';
import { IsEnum, IsIn } from 'class-validator';
import { NotificationStatus } from '../model/notification.enum';

@InputType()
export class UpdateNotificationInput {
  @IsIn([NotificationStatus.Read])
  @IsEnum(NotificationStatus)
  @Field(/* istanbul ignore next */ () => NotificationStatus)
  status: NotificationStatus;
}
