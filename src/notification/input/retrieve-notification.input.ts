import { Field, InputType } from '@hardyscc/nestjs-graphql';
import {
  IsDateString,
  IsNotEmpty,
  IsOptional,
  IsString,
} from 'class-validator';

@InputType()
export class RetrieveNotificationInput {
  @IsNotEmpty()
  @IsString()
  @Field()
  applicationId: string;

  @IsOptional()
  @IsString()
  @Field({ nullable: true })
  subApplicationId?: string;

  @IsOptional()
  @IsString()
  @Field({ nullable: true })
  targetId?: string;

  @IsOptional()
  @IsString()
  @Field({ nullable: true })
  userId?: string;

  @IsOptional()
  @IsDateString()
  @Field({ nullable: true })
  requestDate?: string;
}
