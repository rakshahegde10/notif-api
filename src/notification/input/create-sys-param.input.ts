import { Field, InputType } from '@hardyscc/nestjs-graphql';
import {
  IsBoolean,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

@InputType()
export class CreateSysParamInput {
  @IsNotEmpty()
  @IsString()
  @Field()
  applicationId: string;

  @IsNotEmpty()
  @IsString()
  @Field()
  subApplicationId: string;

  @IsOptional()
  @IsString()
  @Field()
  parameters?: string;

  @IsOptional()
  @IsNumber()
  @Field({ nullable: true })
  retention?: number;

  @IsOptional()
  @IsBoolean()
  @Field({ nullable: true })
  allowGuest?: boolean;

  @IsOptional()
  @IsBoolean()
  @Field({ nullable: true })
  allowSendGeneral?: boolean;

  @IsOptional()
  @IsBoolean()
  @Field({ nullable: true })
  allowSendPersonal?: boolean;

  @IsOptional()
  @IsBoolean()
  @Field({ nullable: true })
  allowSendTarget?: boolean;

  @IsOptional()
  @IsBoolean()
  @Field({ nullable: true })
  active?: boolean;
}
