import { Module } from '@nestjs/common';
import { DynamooseModule } from 'nestjs-dynamoose';
import { NotificationController } from './controller/notification.controller';
import { SysParamController } from './controller/sys-param.controller';
import { NotificationResolver } from './resolver/notification.resolver';
import { SysParamResolver } from './resolver/sys-param.resolver';
import { NotificationSchema } from './schema/notification.schema';
import { SysParamSchema } from './schema/sys-param.schema';
import { NotificationService } from './service/notification.service';
import { SysParamService } from './service/sys-param.service';

@Module({
  imports: [
    DynamooseModule.forFeature([
      {
        name: 'SysParam',
        schema: SysParamSchema,
      },
      {
        name: 'Notification',
        schema: NotificationSchema,
      },
    ]),
  ],
  providers: [
    SysParamService,
    NotificationService,
    SysParamResolver,
    NotificationResolver,
  ],
  controllers: [NotificationController, SysParamController],
})
export class NotificationModule {}
