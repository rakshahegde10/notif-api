import { Body, Controller, Post } from '@nestjs/common';
import { CreateSysParamInput } from '../input/create-sys-param.input';
import { SysParamResolver } from '../resolver/sys-param.resolver';

@Controller('v1/sys-param')
export class SysParamController {
  constructor(private readonly sysParamResolver: SysParamResolver) {}

  @Post('create')
  create(@Body() body: CreateSysParamInput) {
    return this.sysParamResolver.createSysParam(body);
  }
}
