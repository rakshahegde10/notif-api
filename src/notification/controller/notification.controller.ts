import {
  Body,
  Controller,
  Param,
  Patch,
  Post,
  Request,
  UnauthorizedException,
} from '@nestjs/common';
import { plainToClass } from 'class-transformer';
import { validate, ValidatorOptions } from 'class-validator';
import { CreateNotificationInput } from '../input/create-notification.input';
import { RetrieveNotificationInput } from '../input/retrieve-notification.input';
import { UpdateNotificationInput } from '../input/update-notification.input';
import { MyRequest } from '../notification.type';
import { NotificationResolver } from '../resolver/notification.resolver';
import {
  errorsToException,
  extractRequest,
  parseBody,
} from '../util/notification.util';
import { PlainBody } from '../util/plain-body.decorator';

const validatorOptions: ValidatorOptions = { forbidUnknownValues: true };

@Controller('v1/notification')
export class NotificationController {
  constructor(private readonly notificationResolver: NotificationResolver) {}

  @Post('create')
  async create(
    @PlainBody() _body: string | Record<string, unknown>,
    @Request() req: MyRequest,
  ) {
    // handle incorrect content-type
    // TODO : should be delete later
    const body = plainToClass(CreateNotificationInput, parseBody(_body));
    errorsToException(await validate(body, validatorOptions));

    return this.notificationResolver.createNotification(
      body,
      extractRequest(req),
    );
  }

  @Patch('update/:id')
  async update(
    @Param('id') id: string,
    @Body() _body: Record<string, unknown>,
    @Request() req: MyRequest,
  ) {
    // handle incorrect content-type
    // TODO : should be delete later
    const body = plainToClass(UpdateNotificationInput, parseBody(_body));
    errorsToException(await validate(body, validatorOptions));

    let userId = '';

    /* istanbul ignore if */
    if (req.apiGateway && process.env.NODE_ENV !== 'test') {
      const { userid } = req.apiGateway.event.headers;
      if (userid !== undefined) {
        userId = userid;
      } else {
        throw new UnauthorizedException('Invalid userid');
      }
    }

    return this.notificationResolver.updateNotification(
      id,
      userId,
      body,
      extractRequest(req),
    );
  }

  @Post('retrieve')
  async retrieve(
    @Body() _body: Record<string, unknown>,
    @Request() req: MyRequest,
  ) {
    // handle incorrect content-type
    // TODO : should be delete later
    const body = plainToClass(RetrieveNotificationInput, parseBody(_body));
    errorsToException(await validate(body, validatorOptions));

    /* istanbul ignore if */
    if (req.apiGateway && process.env.NODE_ENV !== 'test') {
      const { targetid, userid } = req.apiGateway.event.headers;
      if (body.targetId && body.targetId !== targetid) {
        throw new UnauthorizedException('Invalid targetid');
      }
      if (body.userId && body.userId !== userid) {
        throw new UnauthorizedException('Invalid userid');
      }
    }
    return this.notificationResolver.notification(body, extractRequest(req));
  }
}
