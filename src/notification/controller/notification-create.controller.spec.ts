import { Test, TestingModule } from '@nestjs/testing';
import sysParamData from '../data/sys-param.data';
import { NotificationResolver } from '../resolver/notification.resolver';
import { SysParamResolver } from '../resolver/sys-param.resolver';
import { NotificationService } from '../service/notification.service';
import { SysParamService } from '../service/sys-param.service';
import { NotificationTestImports } from '../test/notification-test.import';
import { buildRequest } from '../util/notification.util';
import { NotificationController } from './notification.controller';
import { SysParamController } from './sys-param.controller';
const applicationId = 'hk.org.ha.testhago2';

let notificationController: NotificationController;
let sysParamController: SysParamController;

beforeAll(async () => {
  const module: TestingModule = await Test.createTestingModule({
    imports: NotificationTestImports,
    providers: [
      SysParamService,
      NotificationService,
      SysParamResolver,
      NotificationResolver,
    ],
    controllers: [NotificationController, SysParamController],
  }).compile();

  notificationController = module.get<NotificationController>(
    NotificationController,
  );
  sysParamController = module.get<SysParamController>(SysParamController);
});

describe('Notification Controller (Create)', () => {
  // create all testing data
  beforeAll(async () => {
    expect(notificationController).toBeDefined();
    expect(sysParamController).toBeDefined();

    await Promise.all(
      sysParamData(applicationId).map(async (input) => {
        const result = await sysParamController.create(input);
        expect(result).toMatchObject(input);
        expect(result.id).toBeDefined();
      }),
    );
  });

  it('hk.org.ha.hago send general - 200', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hk.org.ha.hago',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };
    const result = await notificationController.create(
      input,
      buildRequest({ isAuthorized: false }),
    );
    expect(result).toMatchObject(input);
    expect(result.id).toBeDefined();
  });

  it('hk.org.ha.hago send general: login_required empty - 200', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hk.org.ha.hago',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      createDate: '2020-03-30T01:00:00.000Z',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: '' }),
      }),
    };
    const result = await notificationController.create(
      input,
      buildRequest({ isAuthorized: false }),
    );
    expect(result).toMatchObject(input);
    expect(result.ttl).toEqual(
      new Date(input.createDate).getTime() / 1000 + 31 * 24 * 3600,
    );
    expect(result.id).toBeDefined();
  });

  it('hk.org.ha.hago send personal - 200', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hk.org.ha.hago',
      alert: '0 General 你有一則醫護指引..',
      userId: 'u01',
      targetId: 't01',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'Y' }),
      }),
    };
    const result = await notificationController.create(
      input,
      buildRequest({ isAuthorized: false }),
    );
    expect(result).toMatchObject(input);
    expect(result.id).toBeDefined();
  });

  it('hk.org.ha.hago send general with userId - 400', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hk.org.ha.hago',
      alert: '0 General 你有一則醫護指引..',
      userId: 'u01',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 400 });
    }
  });

  it('hk.org.ha.hago send general with targetId - 403', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hk.org.ha.hago',
      alert: '0 General 你有一則醫護指引..',
      targetId: 't01',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 403 });
    }
  });

  it('hk.org.ha.hago send general with targetId and userId - 403', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hk.org.ha.hago',
      alert: '0 General 你有一則醫護指引..',
      userId: 'u01',
      targetId: 'u01',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 403 });
    }
  });

  it('hk.org.ha.hago send personal without userId - 400', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hk.org.ha.hago',
      alert: '0 General 你有一則醫護指引..',
      userId: 'u01',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'Y' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 400 });
    }
  });

  it('undefined subApplicationId - 403', async () => {
    const input = {
      applicationId,
      subApplicationId: 'undefined',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({}),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 403 });
    }
  });

  it('undefined application - 403', async () => {
    const input = {
      applicationId: 'undefined',
      subApplicationId: 'undefined',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({}),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 403 });
    }
  });

  it('hago undefined login_required - 400', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hago',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({}),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 400 });
    }
  });

  it('appointment not allow send general - 403', async () => {
    const input = {
      applicationId,
      subApplicationId: 'appointment',
      userId: 'u01',
      targetId: 't01',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 403 });
    }
  });

  it('appointment send personal - 200', async () => {
    const input = {
      applicationId,
      subApplicationId: 'appointment',
      userId: 'u01',
      targetId: 't01',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'Y' }),
      }),
    };

    const result = await notificationController.create(
      input,
      buildRequest({ isAuthorized: false }),
    );
    expect(result).toMatchObject(input);
    expect(result.id).toBeDefined();
  });

  it('appointment send personal missing targetId - 400', async () => {
    const input = {
      applicationId,
      subApplicationId: 'appointment',
      userId: 'u01',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'Y' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 400 });
    }
  });

  it('appointment send personal allow guest - 403', async () => {
    const input = {
      applicationId,
      subApplicationId: 'appointment',
      targetId: 't01',
      userId: 'u01',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 403 });
    }
  });

  it('appointment send personal missing targetId - 400', async () => {
    const input = {
      applicationId,
      subApplicationId: 'appointment',
      userId: 'u01',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'Y' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 400 });
    }
  });

  it('rehab send personal - 200', async () => {
    const input = {
      applicationId,
      subApplicationId: 'rehab',
      userId: 'u01',
      targetId: 't01',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'Y' }),
      }),
    };

    const result = await notificationController.create(
      input,
      buildRequest({ isAuthorized: false }),
    );
    expect(result).toMatchObject(input);
    expect(result.id).toBeDefined();
  });

  it('rehab send personal with allowGuest - 200', async () => {
    const input = {
      applicationId,
      subApplicationId: 'rehab',
      userId: 'u01',
      targetId: 't01',
      alert: 'Rehab Personal allow guest mode',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };

    const result = await notificationController.create(
      input,
      buildRequest({ isAuthorized: false }),
    );
    expect(result).toMatchObject(input);
    expect(result.appUserId).toEqual(`${input.applicationId}:${input.userId}`);
    expect(result.id).toBeDefined();
  });

  it('rehab send personal missing targetId - 400', async () => {
    const input = {
      applicationId,
      subApplicationId: 'rehab',
      userId: 'u01',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 400 });
    }
  });

  it('hago allow send personal - 200', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hago',
      userId: 'u01',
      targetId: 't01',
      alert: 'hago Personal message',
      loginRequired: true,
      badge: 1,
      sound: 'default',
      // data: JSON.stringify({
      //   data: JSON.stringify({ ['login_required']: 'Y' }),
      // }),
    };

    const result = await notificationController.create(
      input,
      buildRequest({ isAuthorized: false }),
    );
    expect(result).toMatchObject(input);
    expect(result.id).toBeDefined();
  });

  it('hago allow send general - 200', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hago',
      alert: 'hago General message',
      badge: 1,
      sound: 'default',
      loginRequired: false,
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };

    const result = await notificationController.create(
      input,
      buildRequest({ isAuthorized: false }),
    );
    expect(result).toMatchObject(input);
    expect(result.id).toBeDefined();
  });

  it('hago send personal with allowGuest - 403', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hago',
      userId: 'u01',
      targetId: 't01',
      alert: 'hago personal allow guest',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 403 });
    }
  });

  it('general and personal are false: not allow send personal - 403', async () => {
    const input = {
      applicationId,
      subApplicationId: 'nopush',
      userId: 'u01',
      targetId: 't01',
      alert: 'nopush personal',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'Y' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 403 });
    }
  });

  it('general and personal are false: not allow send general - 403', async () => {
    const input = {
      applicationId,
      subApplicationId: 'nopush',
      alert: 'nopush general',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 403 });
    }
  });

  it('general and personal are false: not allow send general - 403', async () => {
    const input = {
      applicationId,
      subApplicationId: 'nopush',
      alert: 'nopush general',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 403 });
    }
  });

  it('Without SubAppId Send Personal - 200', async () => {
    const input = {
      applicationId,
      alert: 'Send Personal message',
      userId: 'u01',
      targetId: 't01',
      badge: 1,
      sound: 'default',
      loginRequired: true,
    };

    const result = await notificationController.create(
      input,
      buildRequest({ isAuthorized: false }),
    );
    expect(result).toMatchObject(input);
    expect(result.id).toBeDefined();
  });

  it('allow send by TargetId only - 200', async () => {
    const input = {
      applicationId,
      subApplicationId: 'allowTarget',
      alert: 'Send to Target',
      targetId: 't01',
      badge: 1,
      sound: 'default',
      loginRequired: true,
    };

    const result = await notificationController.create(
      input,
      buildRequest({ isAuthorized: false }),
    );
    expect(result).toMatchObject(input);
    expect(result.id).toBeDefined();
  });

  it('hk.org.ha.hago not allow send by TargetId - 403', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hk.org.ha.hago',
      alert: 'Send to Target',
      targetId: 't01',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({
        data: JSON.stringify({ ['login_required']: 'N' }),
      }),
    };

    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 403 });
    }
  });
});
