import { Test, TestingModule } from '@nestjs/testing';
import notificationData from '../data/notification.data';
import sysParamData from '../data/sys-param.data';
import { NotificationStatus } from '../model/notification.enum';
import { NotificationResolver } from '../resolver/notification.resolver';
import { SysParamResolver } from '../resolver/sys-param.resolver';
import { NotificationService } from '../service/notification.service';
import { SysParamService } from '../service/sys-param.service';
import { NotificationTestImports } from '../test/notification-test.import';
import { buildRequest } from '../util/notification.util';
import { NotificationController } from './notification.controller';
import { SysParamController } from './sys-param.controller';

const applicationId = 'hk.org.ha.testhago';

let notificationController: NotificationController;
let sysParamController: SysParamController;
let notificationService: NotificationService;

beforeAll(async () => {
  const module: TestingModule = await Test.createTestingModule({
    imports: NotificationTestImports,
    providers: [
      SysParamService,
      NotificationService,
      SysParamResolver,
      NotificationResolver,
    ],
    controllers: [NotificationController, SysParamController],
  }).compile();

  notificationController = module.get<NotificationController>(
    NotificationController,
  );
  sysParamController = module.get<SysParamController>(SysParamController);

  notificationService = module.get<NotificationService>(NotificationService);
});

describe('Notification Controller', () => {
  // create all testing data
  beforeAll(async () => {
    expect(notificationController).toBeDefined();
    expect(sysParamController).toBeDefined();

    await Promise.all(
      sysParamData(applicationId).map(async (input) => {
        const result = await sysParamController.create(input);
        expect(result).toMatchObject(input);
        expect(result.id).toBeDefined();
      }),
    );

    await Promise.all(
      notificationData(applicationId).map(async (input) => {
        const result = await notificationController.create(
          input,
          buildRequest({ isAuthorized: false }),
        );
        expect(result).toMatchObject(input);
        expect(result.id).toBeDefined();
      }),
    );

    //insert invalid data for testing
    const input = {
      applicationId,
      subApplicationId: 'hk.org.ha.hago',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
      createDate: '2020-03-30T03:43:45.773Z',
      data: JSON.stringify({
        data: JSON.stringify({}),
      }),
    };
    await notificationService.create(input);
  });

  it('General + Rehab', async () => {
    expect(
      await notificationController.retrieve(
        {
          applicationId,
          targetId: 'B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3',
          requestDate: '2020-04-01T00:00:00.000Z',
        },
        buildRequest({ isAuthorized: false }),
      ),
    ).toHaveLength(3);
  });

  it('General + Personal All Mini App', async () => {
    expect(
      await notificationController.retrieve(
        {
          applicationId,
          userId: '2019040913083656642538',
          targetId: 'B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3',
          requestDate: '2020-04-01T00:00:00.000Z',
        },
        buildRequest({ isAuthorized: true }),
      ),
    ).toHaveLength(7);
  });

  it('General + Personal All Mini App (with userId)', async () => {
    expect(
      await notificationController.retrieve(
        {
          applicationId,
          userId: '2019040913083656642538',
          requestDate: '2020-04-01T00:00:00.000Z',
        },
        buildRequest({ isAuthorized: true }),
      ),
    ).toHaveLength(8);
  });

  it('General + Personal All Mini App (userId 1004)', async () => {
    expect(
      await notificationController.retrieve(
        {
          applicationId,
          userId: '1004',
          targetId: 'B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3',
          requestDate: '2020-04-01T00:00:00.000Z',
        },
        buildRequest({ isAuthorized: true }),
      ),
    ).toHaveLength(4);
  });

  it('General Only without personal (userId 1005)', async () => {
    expect(
      await notificationController.retrieve(
        {
          applicationId,
          userId: '1005',
          targetId: 'B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3',
          requestDate: '2020-04-01T00:00:00.000Z',
        },
        buildRequest({ isAuthorized: true }),
      ),
    ).toHaveLength(2);
  });

  it('General Only + Rehab (targetId T1005)', async () => {
    const items = await notificationController.retrieve(
      {
        applicationId,
        targetId: 'T1005',
        requestDate: '2020-04-01T00:00:00.000Z',
      },
      buildRequest({ isAuthorized: true }),
    );
    expect(items).toHaveLength(3);
    expect(items[0].status).toBe(NotificationStatus.New);
    expect(items[0].targetId).toBe('T1005');

    const updatedItem = await notificationController.update(
      items[2].id,
      {
        status: NotificationStatus.Read,
      },
      buildRequest({ isAuthorized: true }),
    );

    expect(updatedItem.status).toBe(NotificationStatus.Read);
  });

  it('General Only + Rehab (targetId T1005) (update Read)', async () => {
    const items = await notificationController.retrieve(
      {
        applicationId,
        targetId: 'T1005',
        requestDate: '2020-04-01T00:00:00.000Z',
      },
      buildRequest({ isAuthorized: true }),
    );
    expect(items).toHaveLength(3);
    expect(items[0].status).toBe(NotificationStatus.New);
    expect(items[0].targetId).toBe('T1005');

    const updatedItem = await notificationController.update(
      items[0].id,
      {
        status: NotificationStatus.Read,
      },
      buildRequest({ isAuthorized: true }),
    );

    expect(updatedItem.status).toBe(NotificationStatus.Read);

    try {
      await notificationController.update(
        'fakeid',
        {
          status: NotificationStatus.Read,
        },
        buildRequest({ isAuthorized: true }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 400 });
    }
  });

  it('Out range of retention filter', async () => {
    expect(
      await notificationController.retrieve(
        {
          applicationId,
        },
        buildRequest({ isAuthorized: true }),
      ),
    ).toHaveLength(0);
  });

  it('Invalid JSON', async () => {
    const input = {
      applicationId,
      subApplicationId: 'hago',
      userId: 'u01',
      targetId: 't01',
      alert: '0 General 你有一則醫護指引..',
      badge: 1,
      sound: 'default',
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 400 });
    }
  });

  it('Test other app param JSON', async () => {
    const input = {
      applicationId,
      subApplicationId: 'dummy',
      alert: '0 General',
      badge: 1,
      sound: 'default',
      data: JSON.stringify({}),
    };
    try {
      await notificationController.create(
        input,
        buildRequest({ isAuthorized: false }),
      );
    } catch (e) {
      expect(e).toMatchObject({ status: 403 });
    }
  });
});
