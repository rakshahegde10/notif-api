import { Args, Mutation, Resolver } from '@hardyscc/nestjs-graphql';
import { CreateSysParamInput } from '../input/create-sys-param.input';
import { SysParam } from '../model/sys-param.model';
import { SysParamService } from '../service/sys-param.service';

@Resolver(() => SysParam)
export class SysParamResolver {
  constructor(private readonly sysParamService: SysParamService) {}

  @Mutation(/* istanbul ignore next */ () => SysParam)
  createSysParam(@Args('input') input: CreateSysParamInput) {
    return this.sysParamService.create(input);
  }
}
