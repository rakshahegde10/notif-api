import {
  Args,
  Context,
  ID,
  Mutation,
  Query,
  Resolver,
} from '@hardyscc/nestjs-graphql';
import {
  BadRequestException,
  ForbiddenException,
  UnauthorizedException,
} from '@nestjs/common';
import moment from 'moment';
import { logger } from '../../util/logger.util';
import { CreateNotificationInput } from '../input/create-notification.input';
import { RetrieveNotificationInput } from '../input/retrieve-notification.input';
import { UpdateNotificationInput } from '../input/update-notification.input';
import { Notification } from '../model/notification.model';
import { SysParam } from '../model/sys-param.model';
import { MyContext } from '../notification.type';
import { NotificationService } from '../service/notification.service';
import { SysParamService } from '../service/sys-param.service';
import {
  array2Map,
  extractLoginRequired,
  extractLoginRequiredForCustomSearchKey,
  extractParameters,
} from '../util/notification.util';

@Resolver(() => Notification)
export class NotificationResolver {
  constructor(
    private readonly sysParamService: SysParamService,
    private readonly notificationService: NotificationService,
  ) {}

  @Mutation(/* istanbul ignore next */ () => Notification)
  async createNotification(
    @Args('input') input: CreateNotificationInput,
    @Context() ctx: MyContext,
  ) {
    if (!input.subApplicationId) {
      logger.info(
        ctx.requestId,
        'input.subApplicationId is undefined and set to default:',
        input.applicationId,
      );
      input.subApplicationId = input.applicationId;
    }
    const sysParams = await this.sysParamService.find(
      input.applicationId,
      input.subApplicationId,
    );
    if (sysParams.length !== 1) {
      logger.info(
        ctx.requestId,
        'ForbiddenException: sysParam invalid',
        'applicationId:',
        input.applicationId,
        'subApplicationId:',
        input.subApplicationId,
        'Input:',
        JSON.stringify(input),
      );
      throw new ForbiddenException();
    }
    const sysParam = sysParams[0];
    logger.info(
      ctx.requestId,
      'save notification for applicationId:',
      sysParam.applicationId,
      'subApplicationId:',
      sysParam.subApplicationId,
    );

    let loginRequired = undefined;
    if (input.loginRequired === undefined) {
      loginRequired = extractLoginRequired(input.data);
    } else {
      loginRequired = input.loginRequired;
    }

    if (sysParam.allowSendGeneral || sysParam.allowSendPersonal) {
      if (loginRequired === undefined) {
        logger.info(
          ctx.requestId,
          'BadRequestException: loginRequired undefined',
          'loginRequired:',
          loginRequired,
          'Input:',
          JSON.stringify(input),
        );
        throw new BadRequestException();
      }
    }

    if (input.userId && input.targetId) {
      // personal
      if (!sysParam.allowSendPersonal || !loginRequired) {
        if (!sysParam.allowGuest) {
          logger.info(
            ctx.requestId,
            'ForbiddenException: not allow guest mode',
            'sending personal message allowSendPersonal:',
            sysParam.allowSendPersonal,
            'loginRequired:',
            loginRequired,
            'allowGuest:',
            sysParam.allowGuest,
            'Input:',
            JSON.stringify(input),
          );
          throw new ForbiddenException();
        }
      }
    } else if (!input.userId && input.targetId) {
      if (!sysParam.allowSendTarget) {
        logger.info(
          ctx.requestId,
          'ForbiddenException: not allow only send by targetId',
          'sending personal message allowSendPersonal:',
          sysParam.allowSendPersonal,
          'loginRequired:',
          loginRequired,
          'allowGuest:',
          sysParam.allowGuest,
          'Input:',
          JSON.stringify(input),
        );
        throw new ForbiddenException();
      }
    } else if (!input.userId && !input.targetId) {
      // general
      if (!sysParam.allowSendGeneral || loginRequired) {
        logger.info(
          ctx.requestId,
          'ForbiddenException: not allow send general',
          'sending general message allowSendGeneral:',
          sysParam.allowSendGeneral,
          'loginRequired:',
          loginRequired,
          'Input:',
          JSON.stringify(input),
        );
        throw new ForbiddenException();
      }
    } else {
      logger.info(
        ctx.requestId,
        'BadRequestException: undefined input',
        'userId:',
        input.userId,
        'targetId:',
        input.targetId,
        'Input:',
        JSON.stringify(input),
      );
      throw new BadRequestException();
    }

    // handling for transition, to be removed in next phase
    input.loginRequired = loginRequired;
    try {
      const result = await this.notificationService.create(input);
      console.log('TRACK_NOTI ', result.id, result.createDate, result.version);
      return result;
    } catch (e) {
      logger.error(e);
      throw e;
    }
  }

  @Mutation(/* istanbul ignore next */ () => Notification)
  async updateNotification(
    @Args('id', { type: /* istanbul ignore next */ () => ID }) id: string,
    @Args('userid', { type: /* istanbul ignore next */ () => ID })
    userid: string,
    @Args('input') input: UpdateNotificationInput,
    @Context() ctx: MyContext,
  ) {
    // print the summary
    logger.info(ctx.requestId, 'isLogin:', ctx.isAuthorized, 'input:', input);

    /* istanbul ignore next */
    if (!ctx.isAuthorized) {
      throw new UnauthorizedException('Not Authorized');
    }
    try {
      const result = await this.notificationService.update(id, userid, input);
      console.log('TRACK_NOTI ', result.id, result.createDate, result.version);
      return result;
    } catch (e) {
      logger.error(e);
      throw e;
    }
  }

  @Query(/* istanbul ignore next */ () => [Notification])
  async notification(
    @Args('input') input: RetrieveNotificationInput,
    @Context() ctx: MyContext,
  ) {
    const requestDate = input.requestDate ?? new Date().toISOString();

    // print the summary
    logger.info(
      ctx.requestId,
      'isLogin:',
      ctx.isAuthorized,
      'requestDate:',
      requestDate,
      'input:',
      input,
    );

    // build The SysParam HashMap by subApplicationId
    const sysParamMap = array2Map<SysParam>(
      await this.sysParamService.findAll(input.applicationId),
      'subApplicationId',
    );

    // calculate the max retention
    const maxRetention = Object.values(sysParamMap).reduce((max, sysParam) => {
      const { retention, allowGuest } = extractParameters(sysParam.parameters);
      sysParam.retention = sysParam.retention ?? retention;
      sysParam.allowGuest = sysParam.allowGuest ?? allowGuest;
      return sysParam.retention! > max ? sysParam.retention! : max;
    }, 0);

    logger.debug(ctx.requestId, 'maxRetention: ' + maxRetention);

    // general notification
    const generalNotifications: Notification[] = (
      await this.notificationService.find(
        { applicationId: input.applicationId, requestDate },
        maxRetention,
      )
    )
      // filter out login required notification
      .filter((item) => {
        const loginRequired = extractLoginRequiredForCustomSearchKey(
          item.customSearchKey,
        );
        /* istanbul ignore next */
        return loginRequired !== undefined
          ? !loginRequired || ctx.isAuthorized
          : true;
      })
      .filter(retentionFilter(sysParamMap, requestDate))
      .sort((l, r) => moment(r.createDate).diff(l.createDate));

    // print out general notifications count
    logger.info(
      ctx.requestId,
      'general notifications count: ' + generalNotifications.length,
    );

    // personal notification - make sure always has targetId
    let personalNotifications: Notification[] = [];
    if (input.targetId) {
      if (ctx.isAuthorized) {
        personalNotifications.push(
          ...(await this.notificationService.find(
            { ...input, requestDate },
            maxRetention,
          )),
        );
      } else {
        // handle allow view by guest
        await Promise.all(
          Object.values(sysParamMap).map(async (sysParam) => {
            if (sysParam.allowGuest) {
              personalNotifications.push(
                ...(await this.notificationService.find(
                  {
                    ...input,
                    subApplicationId: sysParam.subApplicationId,
                    requestDate,
                  },
                  maxRetention,
                )),
              );
            }
          }),
        );

        personalNotifications = personalNotifications.filter((item) => {
          const loginRequired = extractLoginRequiredForCustomSearchKey(
            item.customSearchKey,
          );
          /* istanbul ignore next */
          return loginRequired !== undefined ? !loginRequired : true;
        });
      }
      personalNotifications = personalNotifications
        .filter(retentionFilter(sysParamMap, requestDate))
        .sort((l, r) => moment(r.createDate).diff(l.createDate));
    } else {
      if (ctx.isAuthorized && input.userId) {
        // personal notifications with userId only (undercare user)
        personalNotifications.push(
          ...(await this.notificationService.find(
            { ...input, requestDate },
            maxRetention,
          )),
        );
        personalNotifications = personalNotifications
          .filter(retentionFilter(sysParamMap, requestDate))
          .sort((l, r) => moment(r.createDate).diff(l.createDate));
      }
    }
    // print out personal notifications count
    logger.info(
      ctx.requestId,
      'personal notifications count: ' + personalNotifications.length,
    );

    return personalNotifications.concat(generalNotifications);
  }
}

const retentionFilter = (
  sysParamMap: Record<string, SysParam>,
  requestDate: string,
) => {
  return (item: Notification) => {
    /* istanbul ignore next */
    if (!item.subApplicationId) {
      return false;
    }
    const sysParam = sysParamMap[item.subApplicationId];
    /* istanbul ignore next */
    if (!sysParam) {
      return false;
    }
    return moment(requestDate)
      .subtract(sysParam.retention, 'days')
      .isBefore(item.createDate);
  };
};
