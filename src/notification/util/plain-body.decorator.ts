/* istanbul ignore file */
import {
  BadRequestException,
  createParamDecorator,
  ExecutionContext,
} from '@nestjs/common';
import rawBody from 'raw-body';

export const PlainBody = createParamDecorator(
  async (_, context: ExecutionContext) => {
    const req = context.switchToHttp().getRequest<import('express').Request>();
    if (req.readable) {
      return (await rawBody(req)).toString('utf8').trim();
    }
    if (req.body) {
      return req.body;
    }
    throw new BadRequestException('Invalid body');
  },
);
