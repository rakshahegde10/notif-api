import { BadRequestException } from '@nestjs/common';
import { ValidationError } from 'class-validator';
import { MyContext, MyRequest } from '../notification.type';

const DUMMY_REQUEST_ID = '00000000-0000-0000-0000-000000000000';

export function array2Map<T extends Record<string, any>>(
  array: T[],
  key: string,
): Record<string, T> {
  return array.reduce((map, obj) => {
    map[obj[key]] = obj;
    return map;
  }, {} as Record<string, T>);
}

export function extractLoginRequired(data?: string): boolean | undefined {
  if (data) {
    const l1 = JSON.parse(data);
    if (l1.data) {
      const l2 = JSON.parse(l1.data);
      if (l2['login_required'] !== undefined) {
        return l2['login_required'] === 'Y';
      }
    }
  }
  return undefined;
}

export function extractLoginRequiredForCustomSearchKey(
  data: string,
): boolean | undefined {
  const l1 = JSON.parse(data);
  if (l1['login_required']) {
    return l1['login_required'] === 'Y';
  }
  return undefined;
}

export function extractParameters(parameters?: string) {
  const data = parameters ? JSON.parse(parameters) : {};
  return {
    retention: data['filter-retention'] ?? 0,
    allowGuest: data['must-login'] === 'N',
  };
}

export function extractRequest(request: MyRequest): MyContext {
  /* istanbul ignore next */
  return {
    isAuthorized:
      request.apiGateway?.event.requestContext.authorizer.isAuthorized ===
      'true',
    requestId:
      request.apiGateway?.event.requestContext.requestId ?? DUMMY_REQUEST_ID,
  };
}

export function buildRequest({
  isAuthorized,
  targetid,
  userid,
}: {
  isAuthorized: boolean;
  targetid?: string;
  userid?: string;
}): MyRequest {
  return {
    apiGateway: {
      event: {
        headers: { targetid, userid },
        requestContext: {
          authorizer: { isAuthorized: String(isAuthorized) },
          requestId: DUMMY_REQUEST_ID,
        },
      },
    },
  };
}

export function buildIndexKeys({
  applicationId,
  userId,
  targetId,
}: {
  applicationId: string;
  userId?: string;
  targetId?: string;
}) {
  return {
    appTargetId: `${applicationId}:${targetId ?? '*'}`,
    appUserTargetId: `${applicationId}:${userId ?? '*'}:${targetId ?? '*'}`,
    appUserId: `${applicationId}:${userId ?? '*'}`,
  };
}

export function parseBody(body: object | string) {
  /* istanbul ignore if */
  if (typeof body === 'string') {
    if (body.startsWith('{')) {
      return JSON.parse(body);
    }
    return body;
  }
  const keys = Object.keys(body);
  /* istanbul ignore if */
  if (keys.length > 0 && keys[0].startsWith('{')) {
    return JSON.parse(keys[0]);
  }
  return body;
}

export function errorsToException(errors: ValidationError[]) {
  /* istanbul ignore if */
  if (errors.length > 0) {
    const messages = errors.reduce((arr, obj) => {
      return obj.constraints
        ? [...arr, ...Object.values(obj.constraints)]
        : arr;
    }, [] as string[]);
    throw new BadRequestException(messages);
  }
}
