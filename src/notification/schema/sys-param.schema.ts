import { Schema } from 'dynamoose';

export const SysParamSchema = new Schema({
  id: {
    type: String,
    hashKey: true,
  },
  applicationId: {
    type: String,
    index: {
      name: 'applicationId-subApplicationId-index',
      global: true,
      rangeKey: 'subApplicationId',
    },
  },
  subApplicationId: {
    type: String,
  },
  parameters: {
    type: String,
  },
  retention: {
    type: Number,
  },
  allowGuest: {
    type: Boolean,
  },
  active: {
    type: Boolean,
  },
  createDate: {
    type: String,
  },
  updateDate: {
    type: String,
  },
  version: {
    type: Number,
  },
  allowSendGeneral: {
    type: Boolean,
  },
  allowSendPersonal: {
    type: Boolean,
  },
  allowSendTarget: {
    type: Boolean,
  },
});
