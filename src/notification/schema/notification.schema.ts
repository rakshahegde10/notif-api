import { Schema } from 'dynamoose';

export const NotificationSchema = new Schema({
  appTargetId: {
    type: String,
    hashKey: true,
  },
  appUserTargetId: {
    type: String,
    index: {
      name: 'appUserTargetId-createDate-index',
      global: true,
      rangeKey: 'createDate',
    },
  },
  appUserId: {
    type: String,
    index: {
      name: 'appUserId-createDate-index',
      global: true,
      rangeKey: 'createDate',
    },
  },
  id: {
    type: String,
    index: {
      name: 'id-index',
      global: true,
    },
  },
  applicationId: {
    type: String,
  },
  subApplicationId: {
    type: String,
  },
  targetId: {
    type: String,
  },
  userId: {
    type: String,
  },
  alert: {
    type: String,
  },
  badge: {
    type: Number,
  },
  sound: {
    type: String,
  },
  data: {
    type: String,
  },
  createDate: {
    type: String,
    rangeKey: true,
  },
  customSearchKey: {
    type: String,
  },
  version: {
    type: Number,
  },
  status: {
    type: String,
  },
  loginRequired: {
    type: Boolean,
  },
  ttl: {
    type: Number,
  },
});
