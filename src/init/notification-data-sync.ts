import { InternalServerErrorException } from '@nestjs/common';
import { DynamoDBRecord } from 'aws-lambda';
import AWS from 'aws-sdk';
import { log } from '../util/logger.util';
import { putItem } from './util/dynamo-db.util';

log.setDefaultLevel(process.env.LOG_LEVEL as log.LogLevelDesc);

const tableName = `${process.env.SERVICE}-${process.env.STAGE}-Notification`;

const errorTest = process.env.ERROR_TEST;

exports.handler = async (event: DynamoDBRecord) => {
  log.info('input event:', JSON.stringify(event));

  // For Testing
  log.info('ERROR_TEST:', errorTest);
  if (errorTest) {
    throw new InternalServerErrorException();
  }

  if (event.dynamodb?.NewImage) {
    const attMap = AWS.DynamoDB.Converter.unmarshall(event.dynamodb?.NewImage);
    log.info('attMap', JSON.stringify(attMap));

    const putParam = {
      TableName: tableName,
      Item: attMap,
    };
    try {
      await putItem(putParam);
      if (attMap['status'] !== 'Read') {
        console.log(
          'TRACK_NOTI ',
          attMap['id'],
          attMap['createDate'],
          attMap['version'],
        );
      }
    } catch (e) {
      log.error(e);
      throw e;
    }
  }

  return {
    statusCode: 201,
    body: JSON.stringify('Success'),
  };
};
