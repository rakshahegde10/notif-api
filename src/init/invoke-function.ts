import * as AWS from 'aws-sdk';
import { logger } from '../util/logger.util';

let funcArn =
  'arn:aws:lambda:ap-east-1:537410092339:function:notif-center-api-dev-v2-create-notification';

exports.handler = async (event: any) => {
  console.log('start invoke other lambda input: ' + JSON.stringify(event));

  if (process.env.FUNC_ARN) {
    funcArn = process.env.FUNC_ARN;
  }

  let response;
  try {
    logger.info('start invoke other lambda input: ' + JSON.stringify(event));
    const reqCtx = {
      requestId: 'xxxx-xxxx-xxx-xxx',
    };
    const payload = {
      body: event,
      context: reqCtx,
    };
    const params = {
      FunctionName: funcArn!, // the lambda function we are going to invoke
      InvocationType: 'RequestResponse',
      Payload: JSON.stringify(payload),
    };

    const lambda = new AWS.Lambda();
    if (!lambda) {
      throw new Error('init lambda error');
    }

    console.log('params: ', JSON.stringify(params));
    response = await lambda.invoke(params).promise();

    console.log('StatusCode: ' + response.StatusCode);
    console.log('Result Log: ' + JSON.stringify(response));
  } catch (err) {
    console.log('invoke error: ' + JSON.stringify(err));
  }

  return response;
};
