import { SysParam } from 'src/notification/model/sys-param.model';
import { log, logger } from '../util/logger.util';
import { deleteItem, putItem } from './util/dynamo-db.util';

log.setDefaultLevel(process.env.LOG_LEVEL as log.LogLevelDesc);

// type InitParam = {
//   operation: string;
//   appId: string[];
// };

exports.handler = async (event: any) => {
  try {
    const tableName = `${process.env.SERVICE}-${process.env.STAGE}-SysParam`;

    const jsonArray = (await import('../../seed/sys-param.json')).default;

    const operation = event['operation'] ?? undefined;
    const appIds = event['appIds'] ?? undefined;

    logger.info(null, 'event:operation: ' + operation);
    logger.info(null, 'event:appId: ' + appIds);

    for (let i = 0; i < jsonArray.length; i++) {
      const data: SysParam = jsonArray[i];
      logger.info(null, 'data: ' + JSON.stringify(jsonArray[i]));

      const params = {
        TableName: tableName,
        Item: {
          id: data.id,
          applicationId: data.applicationId,
          subApplicationId: data.subApplicationId,
          parameters: data.parameters,
          active: data.active,
          allowGuest: data.allowGuest,
          allowSendGeneral: data.allowSendGeneral,
          allowSendPersonal: data.allowSendPersonal,
          allowSendTarget: data.allowSendTarget,
          retention: data.retention,
          updateDate: data.updateDate
            ? new Date(data.updateDate).toISOString()
            : new Date().toISOString(),
          createDate: data.createDate
            ? new Date(data.createDate).toISOString()
            : new Date().toISOString(),
          version: data.version,
        },
      };
      await putItem(params);

      //deleteItem
      if (operation === 'd') {
        logger.info(null, 'initParam.operation: ' + operation);
        if (appIds.includes(data.applicationId)) {
          const delParams = {
            TableName: tableName,
            Key: {
              id: data.id,
            },
          };
          logger.info(null, 'delete item: ' + JSON.stringify(data));
          await deleteItem(delParams);
        }
      }

      logger.info(null, 'create SysParam.id: ' + data.id);
    }

    const response = {
      statusCode: 200,
      body: JSON.stringify('Success'),
    };
    return response;
  } catch (err) {
    logger.error(err);
    const response = {
      statusCode: 500,
      body: JSON.stringify(err),
    };

    return response;
  }
};
