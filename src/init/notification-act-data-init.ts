import {
  CloudWatchLogsDecodedData,
  CloudWatchLogsEvent,
} from 'aws-lambda/trigger/cloudwatch-logs';
import moment from 'moment';
import {
  buildIndexKeys,
  extractLoginRequired,
} from 'src/notification/util/notification.util';
import { gunzipSync } from 'zlib';
import { log, logger } from '../util/logger.util';
import { deleteItem, putItem, query } from './util/dynamo-db.util';

log.setDefaultLevel(process.env.LOG_LEVEL as log.LogLevelDesc);

type NotificationData = {
  id: string;
  applicationId: string;
  subApplicationId: string;
  userId?: string;
  targetId?: string;
  badge?: number;
  sound?: string;
  alert: string;
  data: string;
  offsetDay?: number;
  createDate?: string;
};

function parseLogValue(logMsg: string, field: string): string | undefined {
  const pos = logMsg.indexOf(field);
  if (pos > 0) {
    const value = logMsg.substring(
      logMsg.indexOf("'", pos) + 1,
      logMsg.indexOf("',", pos),
    );
    logger.info(null, 'extract field:', field, 'value:', value);
    return value;
  }
  return undefined;
}

exports.handler = async (event: CloudWatchLogsEvent) => {
  let userIdFromLog = undefined;
  let targetIdFromLog = undefined;
  logger.info(null, 'event:', event);

  try {
    if (Object.keys(event).length > 0 && event.awslogs.data) {
      const base64ZippedData = event.awslogs.data;
      const compressedPayload = Buffer.from(base64ZippedData, 'base64');
      const jsonPayload = gunzipSync(compressedPayload).toString('utf8');
      const logEventData: CloudWatchLogsDecodedData = JSON.parse(jsonPayload);
      logger.info(null, logEventData);
      logEventData.logEvents.forEach(function (logEvent) {
        logger.info(null, 'logEvent', logEvent);
        userIdFromLog = parseLogValue(logEvent.message, 'user_id:');
        targetIdFromLog = parseLogValue(logEvent.message, 'device_uuid:');
      });

      if (!targetIdFromLog) {
        logger.info(null, 'Skip CloudWatch Log Event');
        const response = {
          statusCode: 200,
          body: JSON.stringify('Skip CloudWatch Log Event'),
        };
        return response;
      }
    } else {
      logger.info(null, 'without CloudWatch log data to parse');
    }

    const tableName = `${process.env.SERVICE}-${process.env.STAGE}-Notification`;

    const dataArray = (await import('../../seed/notification-act.json'))
      .default;

    for (let i = 0; i < dataArray.length; i++) {
      const data: NotificationData = dataArray[i];

      // check the userid from cloudwatch log event with the data file
      if (data.userId && userIdFromLog && data.userId !== userIdFromLog) {
        logger.info(
          null,
          'test userId not match, skip import test record of userId:',
          userIdFromLog,
        );
        continue;
      }

      let createDt = data.createDate;
      if (!data.createDate) {
        createDt = moment(new Date())
          .subtract(data.offsetDay, 'days')
          .toISOString();
      }

      const { appTargetId, appUserTargetId } = buildIndexKeys(data);
      logger.info(
        null,
        `appTargetId: ${appTargetId} appUserTargetId ${appUserTargetId}`,
      );
      const queryParam = {
        TableName: tableName,
        IndexName: 'id-index',
        KeyConditionExpression: 'id = :v1',
        ExpressionAttributeValues: {
          ':v1': data.id,
        },
      };
      const result = await query(queryParam);

      if (result.Items && result.Items.length > 0) {
        const item = result.Items[0];
        logger.info(
          null,
          `Notification.id: ${data.id} exists ${JSON.stringify(item)}`,
        );

        if (
          // if general message or personal message
          (item.targetId === undefined || item.targetId === targetIdFromLog) &&
          moment(item.createDate).format('YYYY-MM-DD') ===
            moment(createDt).format('YYYY-MM-DD')
        ) {
          logger.info(
            null,
            'skip update test data targetIdFromLog:',
            targetIdFromLog,
            'targetId:',
            item.targetId,
            'userIdFromLog:',
            userIdFromLog,
            'userId:',
            item.userId,
          );
          continue;
        }

        const delParam = {
          TableName: tableName,
          Key: {
            appTargetId: item.appTargetId,
            createDate: item.createDate,
          },
        };
        await deleteItem(delParam);
      }

      if (data.targetId && targetIdFromLog) {
        // set the targetId
        logger.info(
          null,
          'update data to latest targetIdFromLog:',
          targetIdFromLog,
        );
        data.targetId = targetIdFromLog;
      }

      const putParam = {
        TableName: tableName,
        Item: {
          id: data.id,
          applicationId: data.applicationId,
          subApplicationId: data.subApplicationId,
          ...buildIndexKeys(data),
          customSearchKey: JSON.stringify({
            ['login_required']: extractLoginRequired(data.data) ? 'Y' : 'N',
          }),
          alert: data.alert,
          data: data.data,
          userId: data.userId,
          targetId: data.targetId,
          badge: data.badge ? data.badge : 1,
          sound: data.sound ? data.sound : 'default',
          updateDate: new Date().toISOString(),
          createDate: createDt,
          status: 'New',
          version: 1,
        },
      };
      await putItem(putParam);

      logger.info(null, 'create Notification.id: ' + data.id);
    }

    const response = {
      statusCode: 200,
      body: JSON.stringify('Success'),
    };
    return response;
  } catch (err) {
    logger.error(err);
    const response = {
      statusCode: 500,
      body: JSON.stringify(err),
    };

    return response;
  }
};
