import { DocumentClient } from 'aws-sdk/clients/dynamodb';

// connect to local DB if running offline
const options =
  process.env.IS_DDB_LOCAL === 'true'
    ? {
        region: 'localhost',
        endpoint: 'http://localhost:8000',
        accessKeyId: 'xxxx',
        secretAccessKey: 'xxxx',
      }
    : {};

const dynamoDb = new DocumentClient(options);

export const query = (params: DocumentClient.QueryInput) =>
  dynamoDb.query(params).promise();

export const scan = (params: DocumentClient.ScanInput) =>
  dynamoDb.scan(params).promise();

export const getItem = (params: DocumentClient.GetItemInput) =>
  dynamoDb.get(params).promise();

export const putItem = (params: DocumentClient.PutItemInput) =>
  dynamoDb.put(params).promise();

export const updateItem = (params: DocumentClient.UpdateItemInput) =>
  dynamoDb.update(params).promise();

export const deleteItem = (params: DocumentClient.DeleteItemInput) =>
  dynamoDb.delete(params).promise();
