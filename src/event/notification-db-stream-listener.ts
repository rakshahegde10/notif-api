import { DynamoDBStreamEvent } from 'aws-lambda';
import * as AWS from 'aws-sdk';
import log from 'loglevel';

log.setDefaultLevel(process.env.LOG_LEVEL as log.LogLevelDesc);
const funcArn = process.env.XACC_DATA_SYNC_NOTIF_FUNC_ARN;
const processVersion = process.env.XACC_DATA_SYNC_PROCESS_VERSION;

exports.handler = async (event: DynamoDBStreamEvent) => {
  for (const record of event.Records) {
    log.info('DynamoDB Record:', record.dynamodb);

    const lambda = new AWS.Lambda();

    // if INSERT or UPDATE
    if (record.eventName !== 'REMOVE' && record.dynamodb) {
      const payload = record.dynamodb.NewImage;

      if (payload && payload.version.N !== processVersion) {
        log.info(
          `skip the record for record version: ${payload.version.N} target process version ${processVersion}`,
        );
        break;
      }

      const params = {
        FunctionName: funcArn!,
        InvocationType: 'RequestResponse',
        Payload: JSON.stringify(record),
      };

      const response = await lambda.invoke(params).promise();

      log.info('response: ', JSON.stringify(response));
      if (response.FunctionError) {
        throw response.FunctionError;
      }
    }
  }

  return `Successfully processed ${event.Records.length} records.`;
};
