import { APIGatewayProxyHandler } from 'aws-lambda';
import 'source-map-support/register';
import { isNullOrUndefined } from 'util';
import { log, logger } from '../util/logger.util';
import { handleWarmUpEvent } from '../util/warm-up.util';
import { checkToken, ClaimVerifyResult } from './cognito-jwt.verifier';
import { checkHagoUserToken } from './util/hago-auth.util';

log.setDefaultLevel(process.env.LOG_LEVEL as log.LogLevelDesc);

// Help function to generate an IAM policy
export const generatePolicy = (
  principalId: any,
  effect: any,
  resource: any,
  isLoggedIn: boolean,
) => {
  const authResponse: any = {};
  authResponse.principalId = principalId;
  if (effect && resource) {
    const policyDocument: any = {};
    policyDocument.Version = '2012-10-17'; // default version
    policyDocument.Statement = [];
    const statementOne: any = {};
    statementOne.Action = 'execute-api:Invoke'; // default action
    statementOne.Effect = effect;
    statementOne.Resource = resource;
    policyDocument.Statement[0] = statementOne;
    authResponse.policyDocument = policyDocument;
  }
  authResponse.context = {};
  authResponse.context.isAuthorized = isLoggedIn;
  authResponse.context.isAllow = effect;
  return authResponse;
};

export const handler: APIGatewayProxyHandler = async (event) => {
  if (handleWarmUpEvent(event)) return;

  const requestId = event.requestContext.requestId;

  let isValidUserToken = false;
  const headers = event.headers || {};
  const cognitoToken = headers.Authorization;
  logger.info(requestId, 'checkAppToken start');

  const congitTokenResult: ClaimVerifyResult = await checkToken(
    cognitoToken,
    event.resource,
  );
  logger.info(requestId, 'checkAppToken end');

  if (!congitTokenResult.isValid) {
    logger.error(requestId, 'unauthorized appToken');
    throw new Error('Unauthorized');
  }

  const authorizationToken = headers.hagotoken;
  const { targetid, userid } = headers;

  if (isNullOrUndefined(authorizationToken)) {
    logger.info(requestId, 'userToken: null');
    const ret = generatePolicy(targetid, 'Allow', '*', false);
    return ret;
  } else {
    logger.info(requestId, 'checkUserToken start');

    isValidUserToken = await checkHagoUserToken(
      userid,
      targetid,
      authorizationToken,
    );
    logger.info(requestId, 'checkUserToken end');

    if (isValidUserToken) {
      const ret = generatePolicy(userid, 'Allow', '*', true);
      return ret;
    } else {
      logger.error(requestId, 'unauthorized userToken');
      throw new Error('Unauthorized');
    }
  }
};
