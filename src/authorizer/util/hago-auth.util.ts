import CryptoJs from 'crypto-js';
import FormData from 'form-data';
import fetch from 'node-fetch';
import { isNullOrUndefined } from 'util';
import { logger } from '../../util/logger.util';

let clientAccessTokenResult: any = null;

const HAGO_CARER = 'CARER';

const decryptToken = (password: string) => {
  const words = CryptoJs.enc.Base64.parse(password);

  const textString = CryptoJs.enc.Utf8.stringify(words);

  return JSON.parse(textString);
};

const getHaGoClientAccessToken = async () => {
  if (
    clientAccessTokenResult &&
    clientAccessTokenResult.access_token_expr &&
    clientAccessTokenResult.access_token_expr > Date.now()
  ) {
    logger.info(null, 'ncAppToken not yet expired');
    return clientAccessTokenResult.client_access_token;
  }
  clientAccessTokenResult = null;

  const content = {
    ['client_id']: process.env.HAGO_CLIENT_ID,
    ['client_secret']: process.env.HAGO_CLIENT_SECRET,
  };
  const body = new FormData();
  body.append('content', JSON.stringify(content));

  logger.info(null, 'get ncAppToken start');
  logger.info(null, 'get ncAppToken url: ', process.env.HAGO_TOKEN_URL);
  let response = null;
  try {
    response = await fetch(process.env.HAGO_TOKEN_URL!, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
      },
      body,
    });
    logger.info(null, 'get ncAppToken response.status: ', response.status);
    if (response.status >= 400) {
      return null;
    }
  } catch (err) {
    logger.error(err);
    return null;
  }
  logger.info(null, 'get ncAppToken end');

  const tokenResult = await response.json();

  if (tokenResult.status_code !== '0') {
    return null;
  }

  clientAccessTokenResult = tokenResult.result;
  clientAccessTokenResult['access_token_expr'] =
    (clientAccessTokenResult.access_token_expr - 60) * 1000;
  return clientAccessTokenResult.client_access_token;
};

export const checkHagoUserToken = async (
  userId: string,
  deviceUuid: string,
  hagoParams: string,
) => {
  const haGoClientAccessToken = await getHaGoClientAccessToken();

  if (!haGoClientAccessToken) {
    return false;
  }

  logger.debug(null, 'hagoParams: [%s]', hagoParams);
  const data = decryptToken(hagoParams);
  const body = new FormData();

  body.append('content', data.content);
  body.append('en_key', data.en_key);

  logger.info(null, 'checkUserToken url: ', process.env.HAGO_CHECKTOKEN_URL);

  let response = null;
  try {
    response = await fetch(process.env.HAGO_CHECKTOKEN_URL!, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${haGoClientAccessToken}`,
      },
      body,
    });
  } catch (err) {
    logger.error(err);
    return false;
  }

  logger.info(null, 'checkUserToken response.status: ', response.status);
  if (response.status >= 400) {
    return false;
  }
  const haGoGatewayResponse = await response.json();
  logger.info(null, 'checkUserToken response: ', haGoGatewayResponse);
  if (haGoGatewayResponse.status_code !== '0') {
    return false;
  }

  logger.info(null, `check userId:${userId} deviceUuid:${deviceUuid}`);
  let tokenUserId = haGoGatewayResponse.result.user_id;
  if (
    haGoGatewayResponse.result.hago_user_role === HAGO_CARER &&
    haGoGatewayResponse.result.undercare_user_id
  ) {
    tokenUserId = haGoGatewayResponse.result.undercare_user_id;
  }
  logger.info(null, `check userId:${userId} tokenUserId:${tokenUserId}`);
  if (!isNullOrUndefined(userId) && tokenUserId !== userId) {
    logger.error(null, `userId:${userId} mismtached`);
    return false;
  }
  if (
    !isNullOrUndefined(deviceUuid) &&
    haGoGatewayResponse.result.device_uuid !== deviceUuid
  ) {
    logger.error(null, `device_uuid:${deviceUuid} mismatched`);
    return false;
  }
  return true;
};
