import { INestApplication, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { ExpressAdapter } from '@nestjs/platform-express';
import { APIGatewayProxyHandler } from 'aws-lambda';
import { createServer, proxy } from 'aws-serverless-express';
import { eventContext } from 'aws-serverless-express/middleware';
import express from 'express';
import { Server } from 'http';
import { AppModule } from './app.module';
import { CreateNotificationInput } from './notification/input/create-notification.input';
import { MyContext } from './notification/notification.type';
import { NotificationResolver } from './notification/resolver/notification.resolver';
import { logger } from './util/logger.util';
import { handleWarmUpEvent } from './util/warm-up.util';

let cachedServer: Server;
let app: INestApplication;

const INTERNAL_SERVER_ERROR = 500;
const CREATED = 201;

const bootstrapServer = async (): Promise<Server> => {
  const expressApp = express();
  expressApp.use(eventContext());
  app = await NestFactory.create(AppModule, new ExpressAdapter(expressApp));
  app.useGlobalPipes(new ValidationPipe());
  app.enableCors();
  await app.init();
  return createServer(expressApp);
};

export const handler: APIGatewayProxyHandler = async (event, context) => {
  if (!cachedServer) {
    cachedServer = await bootstrapServer();
  }
  return proxy(cachedServer, event, context, 'PROMISE').promise;
};

// PNS2.0 handler to accept lambda-to-lambda invocation
export const createNotification = async (event: {
  body: CreateNotificationInput;
  context: MyContext;
}) => {
  if (handleWarmUpEvent(event)) return;
  if (!cachedServer) {
    cachedServer = await bootstrapServer();
  }
  const notificationResolver = app.get(NotificationResolver);

  try {
    logger.info(event.context.requestId, JSON.stringify(event.body));
    const body = event.body;
    if (body.userId === null) {
      body.userId = '';
    }
    const result = await notificationResolver.createNotification(
      body,
      event.context,
    );
    return { statusCode: CREATED, body: JSON.stringify(result) };
  } catch (err) {
    logger.error(event.context.requestId, JSON.stringify(err));
    const status = err.status
      ? err.status
      : err.statusCode
      ? err.statusCode
      : INTERNAL_SERVER_ERROR;
    return { statusCode: status, body: err.message };
  }
};
