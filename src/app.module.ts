import { GraphQLModule } from '@hardyscc/nestjs-graphql';
import { Module, OnModuleInit } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { DynamooseModule } from 'nestjs-dynamoose';
import { NotificationModule } from './notification/notification.module';
import { extractRequest } from './notification/util/notification.util';
import { log } from './util/logger.util';

@Module({
  imports: [
    ConfigModule.forRoot(),
    GraphQLModule.forRoot({
      autoSchemaFile: true,
      playground: {
        endpoint:
          process.env.IS_NOT_SLS === 'true'
            ? '/graphql'
            : `/${process.env.STAGE}/graphql`,
      },
      context: ({ req }) => extractRequest(req),
    }),
    DynamooseModule.forRoot({
      local: process.env.IS_DDB_LOCAL === 'true',
      aws: { region: process.env.REGION },
      model: {
        create: false,
        prefix: `${process.env.SERVICE}-${process.env.STAGE}-`,
      },
    }),
    NotificationModule,
  ],
})
export class AppModule implements OnModuleInit {
  onModuleInit() {
    log.setDefaultLevel(process.env.LOG_LEVEL as log.LogLevelDesc);
  }
}
