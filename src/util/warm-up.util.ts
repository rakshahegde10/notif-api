import { logger } from './logger.util';

let isColdStart = false;

export function handleWarmUpEvent(event: any) {
  if (!isColdStart) {
    isColdStart = true;
    logger.info(null, 'Lambda is cold start!');
  }
  if (event.source === 'aws.events') {
    return true;
  }
  return false;
}
