/* istanbul ignore file */
import log from 'loglevel';
import moment from 'moment-timezone';

const TIMEZONE = 'Asia/Hong_Kong';
const DATETIME_FORMAT = 'YYYY-MM-DD HH:mm:ss.SSS';
const LOG_TIME_MSG = 'logTime:';
const LOG_GW_REQ_ID = 'apiGwRequestId:';

const printTime = () => moment().tz(TIMEZONE).format(DATETIME_FORMAT);

const logger = {
  info: (requestId: any, ...msg: any[]) => {
    log.info(
      LOG_TIME_MSG,
      printTime(),
      ...(requestId ? [LOG_GW_REQ_ID, requestId] : []),
      ...msg,
    );
  },
  error: (requestId: any, ...msg: any[]) => {
    log.error(
      LOG_TIME_MSG,
      printTime(),
      ...(requestId ? [LOG_GW_REQ_ID, requestId] : []),
      ...msg,
    );
  },
  debug: (requestId: any, ...msg: any[]) => {
    log.debug(
      LOG_TIME_MSG,
      printTime(),
      ...(requestId ? [LOG_GW_REQ_ID, requestId] : []),
      ...msg,
    );
  },
};

export { log, logger };
