echo "Patch SysParam table to add haconnect"
aws dynamodb put-item \
    --table-name notif-center-api-$1-SysParam \
    --item file://./$1/sysparam-haconnect.json \
    --return-consumed-capacity TOTAL \
    --return-item-collection-metrics SIZE \
    --region $2
echo "Done"