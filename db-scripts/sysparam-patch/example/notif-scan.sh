aws dynamodb scan \
        --table-name notif-center-api-$1-Notification \
	--index-name "id-index" \
        --filter-expression "begins_with(createDate, :createDate)" \
        --expression-attribute-values file://notif-scan-values.json \
        --select "COUNT"
