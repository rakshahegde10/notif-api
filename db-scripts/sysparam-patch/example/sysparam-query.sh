aws dynamodb query \
        --table-name notif-center-api-$1-SysParam \
        --index-name "applicationId-subApplicationId-index" \
        --key-condition-expression "applicationId = :appId and subApplicationId = :subAppId" \
        --expression-attribute-values file://sysparam-query-values.json \
        --projection-expression 'id,applicationId,subApplicationId,active'
        --region $2