aws dynamodb update-item \
    --table-name notif-center-api-$1-SysParam \
    --key file://sysparam-payha-id.json \
    --update-expression "SET active = :active" \
    --expression-attribute-values '{":active" : {"BOOL": true}}'  \
    --return-values ALL_NEW \
    --region $2
