aws dynamodb update-item \
    --table-name notif-center-api-$1-Notification \
    --key file://notif-key.json \
    --expression-attribute-names '{"#s" : "status"}' \
    --update-expression "SET #s = :status" \
    --expression-attribute-values '{":status" : {"S": "Delete"}}'  \
    --return-values ALL_NEW
