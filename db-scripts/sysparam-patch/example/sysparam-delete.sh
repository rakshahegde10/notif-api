aws dynamodb delete-item \
    --table-name notif-center-api-$1-SysParam \
    --key '{  "id": { "S": "'$2'" } }' \
    --return-values ALL_OLD \
    --region $3
