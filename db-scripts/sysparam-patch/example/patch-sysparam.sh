if [[ $# -eq 0 ]] ; then
    echo "usage: patch-sysparam.sh dev|sit|fps|prd"
    exit 1
fi

for filename in ./sys-param-*.json; do
    [ -e "$filename" ] || continue
    echo process file: $filename
    cat $filename
    echo response: 
    aws dynamodb put-item \
        --table-name notif-center-api-$1-SysParam \
        --item file://$filename \
        --return-consumed-capacity INDEXES
    echo 
done