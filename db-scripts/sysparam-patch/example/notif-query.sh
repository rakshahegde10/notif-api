aws dynamodb query \
        --table-name notif-center-api-$1-Notification \
        --index-name "appUserTargetId-index" \
        --key-condition-expression "appUserTargetId = :appUserTargetId" \
        --filter-expression "begins_with(createDate, :createDate)" \
        --expression-attribute-values file://notif-query-values.json \
