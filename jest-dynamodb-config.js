/* eslint-disable @typescript-eslint/no-var-requires */
require('dotenv').config();
const yaml = require('js-yaml');
const fs = require('fs');
const { CLOUDFORMATION_SCHEMA } = require('cloudformation-js-yaml-schema');

module.exports = () => {
  const cf = yaml.safeLoad(fs.readFileSync('./resource/dynamodb.yml', 'utf8'), {
    schema: CLOUDFORMATION_SCHEMA,
  });
  let tables = [];
  Object.keys(cf.Resources).forEach((item) => {
    tables.push(cf.Resources[item]);
  });

  tables = tables
    .filter((r) => r.Type === 'AWS::DynamoDB::Table')
    .map((r) => {
      const table = r.Properties;
      if (typeof table.TableName === 'string') {
        table.TableName = table.TableName.replace(
          '${self:service}-${self:provider.stage}',
          `${process.env.SERVICE}-${process.env.STAGE}`,
        );
      }
      if (table.StreamSpecification) {
        table.StreamSpecification.StreamEnabled = true; //errors if not defined on dynamo-local
      }
      if (r.Properties.SSESpecification) {
        delete r.Properties.SSESpecification;
      }
      delete table.TimeToLiveSpecification; //errors on dynamo-local
      return table;
    });

  return {
    tables,
    port: 8001,
    options: ['-sharedDb', '-inMemory'],
    installerConfig: {
      installPath: '.dynamodb',
    },
  };
};
