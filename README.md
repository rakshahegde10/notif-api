# Notification Center API

# Development Machine Setup

[Workstation Setup](doc/WorkstationSetup.md)

# AWS Serverless Tutorial

[AWS Serverless Tutorial](doc/TutorialAws.md)

# AWS Serverless Quick Start

Details can refer to AWS Serverless Quick Start Guide.pdf

Steps to checkout skeleton project and deploy to AWS:

1. pdgitlab account ready
2. AWS Account ready and proper access permission granted
3. Access Key ID and Secret Access Key generated
4. awscli installed
5. aws configuration ready
6. Prepare workspace

```
mkdir workspace
Cd workspace
git clone https://pdgitlab.haitcl.org/hago/notif-center-api.git
cd notif-center-api
npm ci

```

7. Deploy to AWS

```
npm run deploy
```

8. Teardown from AWS

```
npm run remove
```

9. Local Offline Development

```
yarn add -D serverless-offline serverless-dynamodb-local
curl -O http://s3-us-west-2.amazonaws.com/dynamodb-local/dynamodb_local_latest.tar.gz
mkdir .dynamodb
tar -xvzf dynamodb_local_latest.tar.gz -C .dynamodb
rm -f dynamodb_local_latest.tar.gz
npm start

```

## Local Offline Development

```bash
# install dynamodb local
$ npm run ddb:install

# start dynamodb local
$ npm run ddb:start

# start local server
$ npm start

# start local server in watch mode
$ npm run start:watch

# start local server connect to online AWS dynamodb
$ npm run start:online

# re-generate the resources/dynamodb.yml from schemas
$ npm run genres
```

## Unit Testing

```bash
# run unit test
$ npm test

# run unit test with coverage
$ npm run test:cov
```

## Invoke P2 lambda function for create notification records

```bash
sls invoke --function v1-invoke-function --region ap-east-1 --path ./src/notification/data/test-data1.json 
sls invoke --function v1-invoke-function --region ap-east-1 --path ./src/notification/data/test-data2.json 
```

## GraphQL Endpoint Test

- local: http://localhost:3000/graphql
- AWS: https://<your_aws_deployment_id>.execute-api.<region>.amazonaws.com/dev/graphql

```graphql
mutation {
  createNotification(
    input: { targetId: "device1", userId: "user1", content: "Hello World" }
  ) {
    id
  }
}
```

```graphql
query {
  notification {
    id
    targetId
    userId
    content
    createAt
  }
}
```

```graphql
query {
  notificationByUserId(userId: "user1") {
    id
    targetId
    userId
    content
    createAt
  }
}
```

```graphql
query {
  notificationByTargetId(targetId: "device1") {
    id
    targetId
    userId
    content
    createAt
  }
}
```

```graphql
mutation {
  updateNotification(
    id: "1ca7726e-0af8-4ff1-8ef1-4eae97377162"
    input: { content: "Hi" }
  ) {
    id
    targetId
    userId
    content
    createAt
  }
}
```

# Architecture

![GitHub Logo](./doc/img/NotifCenterArch.png)

# API Specification

[API Specification](doc/ApiSpec.md)

# Database Schema

[Database Schema](doc/DbSchema.md)

# Create inital data to Dev Env

aws dynamodb batch-write-item --request-items file://seed/dev/items.json

# Project Structure

| Folder Name                      | Description                                                                                                |
| :------------------------------- | :--------------------------------------------------------------------------------------------------------- |
| /src/helper/                     | Directory for helper script files e.g. dynamoDb operation, response builder, logUtil messageCode constant. |
| /src/helper/\_\_mocks\_\_        | Mock object for dynamoDb                                                                                   |
| /src/notification/               | Directory for core function script files                                                                   |
| /src/notification/\_\_tests\_\_/ | Directory for jest testing script files                                                                    |
| /src/type/                       | Object type definition                                                                                     |
| /src/validation/                 | Object schema validation definition e.g. min, max, required etc                                            |
| /test/                           | Directory for REST Client testing script files                                                             |

# Naming Convention

## Function Name

service name-[stage]-function name

```

create: notif-center-api-dev-create
list: notif-center-api-dev-list
```

## File Name

Typescript File

```

response.ts
create.ts
```

# Source Code Management

[Source Code Management](doc/SourceCodeManagement.md)

# TypeScript Coding Convention

[TypeScript Coding Convention](doc/TypeScriptConvention.md)

# Logging

[Logging](doc/Logging.md)
