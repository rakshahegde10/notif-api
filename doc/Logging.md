---
id: Logging 
title: Logging Guide
sidebar_label: Logging Guide 
---

## Logging Guide

1.	Every log should contain the following information:
- Timestamp
- Log level 
- Meaningful Context
2.	The log should not produce a side effect (e.g. produce unexpected exception).
3.	Log error with details.
4.	Log cannot contains sensitive information.
5.	Correct usage of log levels:
- INFO - Some important messages, event messages which describe when one task is finished. For example: New User created with id xxx. This represents the just for information logs on progress.
- DEBUG - This level is for developers, this is similar to logging the information which you see while you are using debugger or breakpoint, like which function was called and what parameters were passed, etc. It should have the current state so that it can be helpful while debugging and finding the exact issue.
- WARN - These logs are the warnings and not blocking the application to continue, these provide alerts when something is wrong and the workaround is used. e.g wrong user input, retries, etc. The admins should fix these warnings in the future.
- ERROR - Something wrong happened and should be investigated on priority. E.g. database is down the communication with other microservice was unsuccessful or the required input was undefined. The primary audiences are system operators or monitoring systems. (ideally, the production app should have close to zero error logs.)
6.	Provides means to change the log levels dynamically.
7.	Consider to have a centralized logging system.
8.	Take care the performance impact if the application is writing too many logs.
