---
id: ApiSpec
title: API Specification
sidebar_label: API Spec.
---

## API Summary

| ID  | HTTP Action | Resource URI                    | Description                          |
| :-- | :---------- | :------------------------------ | :----------------------------------- |
| (1) | POST        | /\<env>/\<version>/notification/create | Create new notification to database     |
| (2) | POST        | /\<env>/\<version>/notification/retrieve | Retrieve notifications from database |
| (3) | PATCH        | /\<env>/\<version>/notification/update | Update notifications to database |


## URI Versioning

API versioning is using the URI.

    Create:    https://m4bifs6zq3.execute-api.ap-east-1.amazonaws.com/dev/v1/notification/create
    Retreive:  https://m4bifs6zq3.execute-api.ap-east-1.amazonaws.com/dev/v1/notification/retrieve
    Update:    https://m4bifs6zq3.execute-api.ap-east-1.amazonaws.com/dev/v1/notification/update/{id}

# (1) Create Notification

### HTTP Method

POST: /notification/create

### Request Header

| Header Name   | Value                                   | Description |
| :------------ | :-------------------------------------- | :-------------------------------------- |
| Content-Type  | application/json                        ||
| Authorization | Bearer oab3thieWohyai0eoxibaequ0Owae9oh | Client Access Token for PNS issued by AWS Cognito |


### Request Payload

| Input Parameter  | Data Type | Value                                | Description                                                                                                                     |
| :--------------- | :-------- | :----------------------------------- | :------------------------------------------------------------------------------------------------------------------------------ |
| applicationId    | String    | hk.org.ha.hago                       | The application ID defined in PNS (should be same as the mobile app’s bundle ID).                                               |
| subApplicationId | String    | bookha                               | (Optional) Integrated Mini-App ID                                                                                                |
| targetId         | String    | 4D8CA4A3-8B00-43F1-ADDF-ADA59E55DDAD | (Optional) FP defined unique ID to identify mobile user (e.g. the login ID or the booking number). \*\*\*Device UUID from HA Go |
| userId           | String    | 2019040913083656642538              | (Optional) FP defined unique ID to identify mobile user (e.g. the login ID).                                                    |
| alert            | String    | Notification text content            | Text of the notification content.                                                                                               |
| badge            | Numeric   | 1                                    | (Optional) Number that appears as the badge on the application icon.                                                            |
| sound            | String    | default                              | (iOS) Name of the sound file in the application bundle.                                                                         |
| data             | String    | Data of the notification             | (Optional) Custom payload data                                                                                                  |

### Request Example

    {
        "applicationId" : "hk.org.ha.hago",
        "subApplicationId" : "bookha",
        "targetId" : "4D8CA4A3-8B00-43F1-ADDF-ADA59E55DDAD",
        "userId" : "2019040913083656642538",
        "alert" : "HA Go - Test Alert",
        "sound" : "default",
        "badge" : 1,
        "data" : "{ \"custom_alert\":null
                    \"login_required\":\"N\",
                    \"custom_alert_setting\":null,
                    \"app_id\":\"bookha\",
                    \"content\":\"{\"ActHeaderId\":70892,\"ActDetailId\":2,\"ActScheduleId\":5,\"Message\":\"\",\"Type\":\"video\",\"AccessToken\":\"abcdefg\"}\"}"
    }

### Response

Return status code 200 and the newly created item.

| Output Parameter                 | Data Type | Value                                | Description                                                                                  |
| :------------------------------- | :-------- | :----------------------------------- | :------------------------------------------------------------------------------------------- |
| id                               | String    | 4b248b3c-c7f9-422b-9e8f-7a574cc4c685 | Generated UUID                                                                               |
| appUserTargetId                  | String    | hk.org.ha.hago:2019040913083656642538:B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3            | Index field for seaching by applicationId:userId:targetId                                                                   |
| appTargetId                      | String    | hk.org.ha.hago:B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3                                   | Index field for seaching by applicationId:targetId                                                                   |
| \*follows the request parameters | \*        | \*                                   | The response parameters of the created item that follow the request parameters stated above. |
| customSearchKey                  | String    | {\"login_required\":\"Y\"}           | Generic field for custom search key                                                                    |
| createDate                       | String    | 2010-12-21T17:42:34+08:00            | Object creation timestamp                                                                    |
| version                          | Numeric   | 1                                    | Object version                                                                    |
| Status                           | String    | New                                  | Status of Notification is "New"                                                                    |


### Response Example

**Success – 200**

    {
        "id" : "4b248b3c-c7f9-422b-9e8f-7a574cc4c685",
        "appUserTargetId": "hk.org.ha.hago:2019040913083656642538:B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3",
        "appTargetId": "hk.org.ha.hago:B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3",
        "applicationId" : "hk.org.ha.hago",
        "subApplicationId" : "bookha",
        "targetId" : "4D8CA4A3-8B00-43F1-ADDF-ADA59E55DDAD",
        "userId" : "2019040913083656642538",
        "alert" : "HA Go - Test Alert",
        "sound" : "default",
        "badge" : 1,
        "data" : "{ \"custom_alert\":null
                    \"login_required\":\"N\",
                    \"custom_alert_setting\":null,
                    \"app_id\":\"bookha\",
                    \"content\":\"{\"ActHeaderId\":70892,\"ActDetailId\":2,\"ActScheduleId\":5,\"Message\":\"\",\"Type\":\"video\",\"AccessToken\":\"abcdefg\"}\"}",
        "customSearchKey": "{\"login_required\":\"Y\"}",
        "createDate" : "2010-12-21T17:42:34+08:00",
        "version" : 1,
        "status": "New"
    }

**Failure – 400**

    {
        "status" : false,
        "errorMsg" : "alert is a required field"
    }

**Failure – 401**

    {
        "message": "Unauthorized"
    }

**Failure – 500**

    {
        "status" : false
    }

**Failure – 403**

    {
         "statusCode": 403,
         "message": "Forbidden"
    }
    
---

# (2) Retrieve Notification

### HTTP Method

POST: /notification/retrieve

### Request Header

| Header Name   | Value                                   | Description |
| :------------ | :-------------------------------------- | :-------------------------------------- |
| Content-Type  | application/json                        ||
| Authorization | oab3thieWohyai0eoxibaequ0Owae9oh | Client Access Token for HA Go Mobile issued by AWS Cognito |
| targetid      | 4D8CA4A3-8B00-43F1-ADDF-ADA59E55DDAD                       | mobile client submitted target Id to check against the encrypted device_uuid in User Access Token |
| hagotoken     | eyJraWQiOiIzVXRLS2xJcTBMbjFqdzdYMUt1eXV<br>sUXhyaE15MWV2OEtXalZscklSaGVJPSI...  | (Optional) User Access Token issued by HA Go|
| userid        | 2019040913083656642538                           | (Optional) mobile client submitted user ID to check against the encrypted user_id in User Access Token|

\*\*Access Token Issue by oAuth provider, HA Go Authentication Service.

```
hagotoken: Base64 encoded '{"content": "encrypted(hago_user_access_token)", "en_key" :"AES encryptio key"}'

```

### Request Payload

| Input Parameter  | Data Type | Value                                | Description                                                                                                    |
| :--------------- | :-------- | :----------------------------------- | :------------------------------------------------------------------------------------------------------------- |
| applicationId    | String    | hk.org.ha.hago                       | The application ID defined in PNS (should be same as the mobile app’s bundle ID).                              |
| subApplicationId | String    | appointment                          | (Optional) Mini-App ID                                                                          |
| targetId         | String    | 4D8CA4A3-8B00-43F1-ADDF-ADA59E55DDAD | (Optional) FP defined unique ID to identify mobile user (e.g. the login ID or the booking number). Device UUID |
| userId           | String    | 2019040913083656642538              | (Optional) userid                                                                                              |

### Request Example

    {
        "applicationId" : "hk.org.ha.hago",
        "targetId" : "4D8CA4A3-8B00-43F1-ADDF-ADA59E55DDAD"
        "userId" : "2019040913083656642538"
    }

    {
        "applicationId" : "hk.org.ha.hago",
        "targetId" : "4D8CA4A3-8B00-43F1-ADDF-ADA59E55DDAD"
    }


### Response

**Return status code 200 and list of item.**

| Output Parameter  | Data Type | Value                                | Description                                                                                                                               |
| :---------------- | :-------- | :----------------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------- |
| id                | String    | 496cecd0-a534-47ee-b046-369de12915ce | Generated UUID                                                                                                                            |
| appUserTargetId   | String    | hk.org.ha.hago:2019040913083656642538:B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3            | Index field for seaching by applicationId:userId:targetId                                                                   |
| appTargetId       | String    | hk.org.ha.hago:B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3                                   | Index field for seaching by applicationId:targetId                                                                   |
| applicationId     | String    | hk.org.ha.hago                       | The application ID defined in PNS (should be same as the mobile app’s bundle ID).                                                         |
| requesterId       | String    | bookha                               | (Optional) Integrated Mini-App ID                                                                                                          |
| targetId          | String    | 4D8CA4A3-8B00-43F1-ADDF-ADA59E55DDAD | (Optional) FP defined unique ID to identify mobile user (e.g. the login ID or the booking number). \*\*\*topic name for broadcast message |
| alert             | String    | Notification text content            | Text of the notification content.                                                                                                         |
| badge             | Numeric   | 1                                    | (Optional) Number that appears as the badge on the application icon.                                                                      |
| sound             | String    | default                              | (iOS) Name of the sound file in the application bundle.                                                                                   |
| data              | String    | Data of the notification             | (Optional) Custom payload data                                                                                                            |  | It will be presented as data.sound. Useful for Android. |
| customSearchKey   | String    | {\"login_required\":\"Y\"}           | Generic field for custom search key                                                                    |
| createDate        | String    | 2010-12-21T17:42:34+08:00            | Object creation timestamp                                                                    |
| version           | Numeric   | 1                                    | Object version                                                                    |

### Response Example

**Success – 200**

    [
        {
            "targetId": "*",
            "version": 1,
            "appTargetId": "hk.org.ha.testhago:*",
            "customSearchKey": "{\"login_required\":\"N\"}",
            "badge": 1,
            "alert": "[General] 3 Test message",
            "appUserTargetId": "hk.org.ha.testhago:*:*",
            "createDate": "2020-04-14T14:10:28.305+08:00",
            "sound": "default",
            "data": "{\"sound\":\"default\",\"alert\":\"You have one notification scheduled at 06:45 PM.\",\"data\":\"{\\\"custom_alert\\\":\\\"N\\\",\\\"login_required\\\":\\\"N\\\",\\\"custom_alert_setting\\\":null,\\\"app_id\\\":\\\"rehab\\\",\\\"content\\\":\\\"{\\\\\\\"ActHeaderId\\\\\\\":70206,\\\\\\\"ActDetailId\\\\\\\":1,\\\\\\\"ActScheduleId\\\\\\\":4,\\\\\\\"Type\\\\\\\":\\\\\\\"video\\\\\\\",\\\\\\\"Message\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"AccessToken\\\\\\\":\\\\\\\"4MAJRHH1bqbO81GHCjayrxv6hfM6QoqmWz9RkiZl96auqdnHU7P9GnJuxn9j0WG6hE7biV1Py40rZHDHZpZvhMDcTvyYixyKZokKtB4zrFz5N99Rh52MNLUHgS8tVnio\\\\\\\"}\\\"}\",\"badge\":1}",
            "userId": "*",
            "subApplicationId": "hk.org.ha.hago",
            "id": "b879337b-17f5-47fc-8049-185b7db54048",
            "applicationId": "hk.org.ha.testhago"
        },
        {
            "targetId": "*",
            "version": 1,
            "appTargetId": "hk.org.ha.testhago:*",
            "customSearchKey": "{\"login_required\":\"N\"}",
            "badge": 1,
            "alert": "[General] 2 Test message",
            "appUserTargetId": "hk.org.ha.testhago:*:*",
            "createDate": "2020-04-14T14:10:22.025+08:00",
            "sound": "default",
            "data": "{\"sound\":\"default\",\"alert\":\"You have one notification scheduled at 06:45 PM.\",\"data\":\"{\\\"custom_alert\\\":\\\"N\\\",\\\"login_required\\\":\\\"N\\\",\\\"custom_alert_setting\\\":null,\\\"app_id\\\":\\\"rehab\\\",\\\"content\\\":\\\"{\\\\\\\"ActHeaderId\\\\\\\":70206,\\\\\\\"ActDetailId\\\\\\\":1,\\\\\\\"ActScheduleId\\\\\\\":4,\\\\\\\"Type\\\\\\\":\\\\\\\"video\\\\\\\",\\\\\\\"Message\\\\\\\":\\\\\\\"\\\\\\\",\\\\\\\"AccessToken\\\\\\\":\\\\\\\"4MAJRHH1bqbO81GHCjayrxv6hfM6QoqmWz9RkiZl96auqdnHU7P9GnJuxn9j0WG6hE7biV1Py40rZHDHZpZvhMDcTvyYixyKZokKtB4zrFz5N99Rh52MNLUHgS8tVnio\\\\\\\"}\\\"}\",\"badge\":1}",
            "userId": "*",
            "subApplicationId": "hk.org.ha.hago",
            "id": "034d5868-5de8-4ffa-a797-25ab07f525e1",
            "applicationId": "hk.org.ha.testhago"
        }
    ]

**Success – 200**

    []
    
**Failure – 401**

    {
        "message": "Unauthorized"
    }
    
# (3) Update Notification

### HTTP Method

PATCH: /notification/update/{id}

### Request Header
| Header Name   | Value                                   | Description |
| :------------ | :-------------------------------------- | :-------------------------------------- |
| Content-Type  | application/json                        ||
| Authorization | oab3thieWohyai0eoxibaequ0Owae9oh | Client Access Token for HA Go Mobile issued by AWS Cognito |
| targetid      | B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3                       | mobile client submitted target Id to check against the encrypted device_uuid in User Access Token |
| hagotoken     | eyJraWQiOiIzVXRLS2xJcTBMbjFqdzdYMUt1eXV<br>sUXhyaE15MWV2OEtXalZscklSaGVJPSI...  | User Access Token issued by HA Go|
| userid        | 2020021915525605014883                           | mobile client submitted user ID to check against the encrypted user_id in User Access Token and to Update the Notification for that user ID|


\*\*Access Token Issue by oAuth provider, HA Go Authentication Service.

```
hagotoken: Base64 encoded '{"content": "encrypted(hago_user_access_token)", "en_key" :"AES encryptio key"}'

```
### Request Payload

| Input Parameter  | Data Type | Value                                | Description                                                                                                    |
| :--------------- | :-------- | :----------------------------------- | :------------------------------------------------------------------------------------------------------------- |
| status    | String    | "Read"                      | Updates notification's status field from "New" to "Read"                             |

### Request Example

    {
        "status" : "Read"
    }

### Response

**Return status code 200 and list of item.**

| Output Parameter  | Data Type | Value                                | Description                                                                                                                               |
| :---------------- | :-------- | :----------------------------------- | :---------------------------------------------------------------------------------------------------------------------------------------- |
| id                | String    | affba7e7-8be5-4101-b12d-4ffe7dafa0a9 | Generated UUID                                                                                                                            |
| appUserTargetId   | String    | hk.org.ha.testhago:2020021915525605014883:B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3            | Index field for seaching by applicationId:userId:targetId                                                                   |
| appTargetId       | String    | hk.org.ha.testhago:B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3                                   | Index field for seaching by applicationId:targetId                                                                   |
| applicationId     | String    | hk.org.ha.testhago                       | The application ID defined in PNS (should be same as the mobile app’s bundle ID).                                                         |
| subApplicationId  | String    | hk.org.ha.hago                       | Integrated Mini-App ID                                                             |
| targetId          | String    | B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3 | FP defined unique ID to identify mobile user (e.g. the login ID or the booking number). \*\*\*topic name for broadcast message |
| alert             | String    | Testing Message - personal           | Text of the notification content.                                                                                                         |
| badge             | Numeric   | 1                                    | (Optional) Number that appears as the badge on the application icon.                                                                      |
| sound             | String    | default                              | (iOS) Name of the sound file in the application bundle.                                                                                   |
| data              | String    | Data of the notification             | (Optional) Custom payload data                                                                                                            |  | It will be presented as data.sound. Useful for Android. |
| customSearchKey   | String    | {\"login_required\":\"Y\"}           | Generic field for custom search key                                                                    |
| createDate        | String    | 2020-06-19T03:16:05.623Z             | Object creation timestamp                                                                    |
| version           | Numeric   | 1                                    | Object version                                                                    |
| loginRequired     | Boolean   | true                                 | User is logged in                                                                    |
| status            | String    | Read                                 | Notification is read and the status is changed from "New" to "Read"                                                                    |


### Response Example

**Success – 200**

    {
        "targetId": "B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3",
        "version": 1,
        "appTargetId": "hk.org.ha.testhago:B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3",
        "customSearchKey": "{\"login_required\":\"N\"}",
        "alert": "Testing Message - personal another RH02",
        "badge": 1,
        "loginRequired": true,
        "status": "Read",
        "appUserTargetId": "hk.org.ha.testhago:2020021915525605014883:B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3",
        "createDate": "2020-06-19T03:16:05.623Z",
        "sound": "default",
        "data": "{\"sound\":\"default\",\"alert\":\"Bill #BTKO2000031860U is available for payment. Click here to pay now.\",\"data\":\"{\\\"custom_alert\\\":\\\"N\\\",\\\"login_required\\\":\\\"\\\",\\\"custom_alert_setting\\\":null,\\\"app_id\\\":\\\"payha\\\",\\\"content\\\":\\\"eyJpbnZvaWNlSWQiOiI5NzEzOTQiLCAiZW5jb3VudGVyVHlwZSI6Ik90aGVycyIsImludm9pY2VOdW1iZXIiOiJCVEtPMjAwMDAzMTg2MFUifQ==\\\"}\",\"badge\":1}",
        "userId": "2020021915525605014883",
        "subApplicationId": "hk.org.ha.hago",
        "id": "affba7e7-8be5-4101-b12d-4ffe7dafa0a9",
        "applicationId": "hk.org.ha.testhago"
    }

**Failure – 401**

    {
        "message": "Unauthorized"
    }
    
**Failure – 403**

    {
         "statusCode": 403,
         "message": "Forbidden"
    }


## Common HTTP Response Return Code

| HTTP STATUS Code | Description                                                                                                                                                                                                                                                                                                                                                                                                   |
| :--------------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| 200              | OK <br> Successful. <br> Standard response for successful HTTP requests. The actual response will depend on the request method used. In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request, the response will contain an entity describing or containing the result of the action.                                                                 |
| 400              | Bad Request<br>The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax, size too large, invalid request message framing, or deceptive request routing).                                                                                                                                                                                             |
| 401              | Unauthorized<br> Specifically, for use when authentication is required and has failed or has not yet been provided. The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource. See Basic access authentication and Digest access authentication. 401 semantically means "unauthenticated", i.e. the user does not have the necessary credentials. |
| 403              | Forbidden<br> Application not registered. Application try to access to properties not belong to an App.                                                                                                                                                                                                                                                                                                       |
| 404              | Not Found<br> The requested resource could not be found but may be available in the future. Subsequent requests by the client are permissible.                                                                                                                                                                                                                                                                |
| 405              | Method Not Allowed<br> The resource doesn’t support the specified HTTP verb.                                                                                                                                                                                                                                                                                                                                  |
| 500              | Internal Server Error<br> Servers are not working as expected. The request is probably valid but needs to be requested again later.                                                                                                                                                                                                                                                                           |
| 503              | Service Unavailable                                                                                                                                                                                                                                                                                                                                                                                           |
