---
id: tutorial-sls-aws
title: Serverless AWS Quick Start
sidebar_label: Serverless AWS Quick Start
---

## Quick Start

### Setup AWS Credentials

1. [Sign up for an AWS account](https://serverless.com/framework/docs/providers/aws/guide/credentials#sign-up-for-an-aws-account)

2. Login to your AWS account and go to the **Identity & Access Management (IAM)** page.

3. Click on **Users** and then **Add user**. Enter a name in the first field to remind you this User is related to the Serverless Framework, like `serverless-admin`. Enable **Programmatic access** by clicking the checkbox. Click **Next** to go through to the Permissions page. Click on **Attach existing policies directly**. Search for and select **AdministratorAccess** then click **Next: Review**. Check to make sure everything looks good and click **Create user**.

4. View and copy the **API Key & Secret** to a temporary place. You'll need it in the next step.

### Setup Workstation

1. Install AWS CLI

   - Windows: `choco install awscli`
   - MacOS: `brew install awscli`

2. Config AWS CLI

   ```sh
   aws configure
   ```

   Input:

   ```sh
   AWS Access Key ID [****************TKYQ]:
   AWS Secret Access Key [****************yNO2]:
   Default region name [None]:
   Default output format [None]:
   ```

   > Please enter your **AWS Access Key ID** and **AWS Secret Access Key**

### Create and Deploy an AWS NodeJS TypeScript Project

1. Install Serverless CLI

   ```sh
   npm install -g serverless
   ```

2. Create Serverless project using `aws-nodejs-typescript` template

   ```sh
   serverless create -t aws-nodejs-typescript -p <your_project>
   ```

3. Deploy to AWS

   ```sh
   cd <your_project>
   yarn install
   serverless deploy
   ```

   Output:

   ```sh
    ...
    endpoints:
      GET - https://mrbsv5x2ye.execute-api.us-east-1.amazonaws.com/dev/hello
    functions:
      hello: <your_project>-dev-hello
    ...
   ```

   > You can test the provided endpoints from above output

4. Teardown from AWS

   ```sh
   serverless remove
   ```

## Local Offline Development

### Install and Setup

1. Install Serverless offline & Serverless DynamoDB local plugin

   ```sh
   yarn add -D serverless-offline serverless-dynamodb-local
   ```

2. Update the **serverless.yml**

   ```yaml
   custom:
     ...
     dynamodb:
       stages:
         - dev
       start:
         port: 8000
         inMemory: true
         migrate: true

   plugins:
     ...
     - serverless-dynamodb-local
     - serverless-offline
   ```

3. Download the DynamoDB runtime

   ```sh
   serverless dynamodb install
   ```

   > If you are using http proxy `Error in downloading Dynamodb` may occur,
   > please run the following script to manually download the latest DynamoDB binary
   >
   > ```sh
   > curl -O http://s3-us-west-2.amazonaws.com/dynamodb-local/dynamodb_local_latest.tar.gz
   > mkdir .dynamodb
   > tar -xvzf dynamodb_local_latest.tar.gz -C .dynamodb
   > rm -f dynamodb_local_latest.tar.gz
   > ```

4. Start the offline server with DynamoDB

   ```sh
   serverless offline start
   ```

   > You can test the function using http://localhost:3000/hello

### Implement RESTful Functions

1. Update the **serverless.yaml**

   ```yaml
   provider:
     ...
     stage: ${opt:stage, 'dev'}
     region: ${opt:region, 'us-east-1'}
     iamRoleStatements:
       - Effect: Allow
         Action:
           - dynamodb:DescribeTable
           - dynamodb:Query
           - dynamodb:Scan
           - dynamodb:GetItem
           - dynamodb:PutItem
           - dynamodb:UpdateItem
           - dynamodb:DeleteItem
         Resource: arn:aws:dynamodb:${self:provider.region}:*:*

   functions:
     ...
     create:
       handler: create.main
       events:
         - http:
             path: notifications
             method: post
             cors: true
     list:
       handler: list.main
       events:
         - http:
             path: notifications
             method: get
             cors: true

   resources:
     ...
     Resources:
       Notification:
         Type: AWS::DynamoDB::Table
         DeletionPolicy: Delete
         Properties:
           TableName: Notification
           AttributeDefinitions:
             - AttributeName: id
               AttributeType: S
             - AttributeName: registrationId
               AttributeType: S
           KeySchema:
             - AttributeName: id
               KeyType: HASH
           ProvisionedThroughput:
             ReadCapacityUnits: 1
             WriteCapacityUnits: 1
           GlobalSecondaryIndexes:
             - IndexName: registrationId-index
               KeySchema:
                 - AttributeName: registrationId
                   KeyType: HASH
               Projection:
                 ProjectionType: ALL
               ProvisionedThroughput:
                 ReadCapacityUnits: 1
                 WriteCapacityUnits: 1
   ```

2. Add **helper.ts**

   ```ts
   import { DocumentClient } from 'aws-sdk/clients/dynamodb';

   export const dynamoDb = new DocumentClient(
     process.env.IS_DDB_LOCAL === 'true'
       ? {
           region: 'localhost',
           endpoint: 'http://localhost:8000',
         }
       : {},
   );

   const buildResponse = (statusCode: number, body: any) => ({
     statusCode: statusCode,
     headers: {
       'Access-Control-Allow-Origin': '*',
       'Access-Control-Allow-Credentials': true,
     },
     body: JSON.stringify(body),
   });

   export const success = (body: any) => buildResponse(200, body);

   export const failure = (body: any) => buildResponse(500, body);
   ```

3. Install `uuid` packages

   ```sh
   yarn add uuid
   yarn add -D @types/uuid
   ```

4. Add **create.ts**

   ```ts
   import { APIGatewayProxyHandler } from 'aws-lambda';
   import 'source-map-support/register';
   import uuidv4 from 'uuid/v4';
   import { dynamoDb, failure, success } from './helper';

   export const main: APIGatewayProxyHandler = async (event) => {
     const data = JSON.parse(event.body);

     const params = {
       TableName: 'Notification',
       Item: {
         id: uuidv4(),
         registrationId: data.registrationId,
         content: data.content,
         createAt: Date.now(),
       },
     };

     try {
       await dynamoDb.put(params).promise();
       return success(params.Item);
     } catch (e) {
       return failure({ status: false });
     }
   };
   ```

5. Add **list.ts**

   ```ts
   import { APIGatewayProxyHandler } from 'aws-lambda';
   import 'source-map-support/register';
   import { dynamoDb, failure, success } from './helper';

   export const main: APIGatewayProxyHandler = async (event) => {
     const registrationId =
       event.queryStringParameters &&
       event.queryStringParameters.registrationId;

     if (!registrationId) {
       return failure({
         status: false,
         error: "missing query parameter 'registrationId'",
       });
     }

     const params = {
       TableName: 'Notification',
       IndexName: 'registrationId-index',
       KeyConditionExpression: 'registrationId = :registrationId',
       ExpressionAttributeValues: {
         ':registrationId': registrationId,
       },
     };

     try {
       const result = await dynamoDb.query(params).promise();
       return success(result.Items);
     } catch (e) {
       return failure({ status: false });
     }
   };
   ```

### Testing the RESTful Functions

1. Create notification

   ```sh
   curl -X POST "http://localhost:3000/notifications" -d "{\"registrationId\": \"device1\",\"content\": \"Testing message\"}"
   ```

   Result:

   ```json
   {
     "id": "7864815d-0dcb-445a-95e4-1180bc1ecbf0",
     "registrationId": "device1",
     "content": "Testing message",
     "createAt": 1575430705455
   }
   ```

2. Notification listing

   ```sh
   curl -X GET "http://localhost:3000/notifications?registrationId=device1"
   ```

   Result:

   ```json
   [
     {
       "registrationId": "device1",
       "id": "7864815d-0dcb-445a-95e4-1180bc1ecbf0",
       "content": "Testing message",
       "createAt": 1575430705455
     }
   ]
   ```
