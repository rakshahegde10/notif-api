## dev
```
Save Notification API: https://m4bifs6zq3.execute-api.ap-east-1.amazonaws.com/dev/v1/notification/create
Retrieve Notification API: https://m4bifs6zq3.execute-api.ap-east-1.amazonaws.com/dev/v1/notification/retrieve

Pool Id: ap-southeast-1_iRJB3lJl4
Pool ARN: arn:aws:cognito-idp:ap-southeast-1:900276383916:userpool/ap-southeast-1_iRJB3lJl4
Amazon Cognito Domain: https://cms-dev.auth.ap-southeast-1.amazoncognito.com/oauth2/token

[cms-cognito-dev-pns-client]
App Client ID: 2ktu7acfmnbnkm3mvm9i4hbt4k
App Client Secret: 62qg9658d9bs512qk4tae55ratdrajjr6q45l3b2j3alt7o58l2

[cms-cognito-dev-hago-client]
App Client ID: 2h3m5iu24gvot8rp29t8eqttbl
App Client Secret: j7etk97tf0hi4oigt273rbpjkub0g3kd47555h6rrp300sss8p6

```

## sit
```
Save Notification API: https://12cokui17j.execute-api.ap-east-1.amazonaws.com/sit/v1/notification/create
Retrieve Notification API: https://12cokui17j.execute-api.ap-east-1.amazonaws.com/sit/v1/notification/retrieve

Pool Id: ap-southeast-1_qDdEa4ZHy
Pool ARN: arn:aws:cognito-idp:ap-southeast-1:303498446923:userpool/ap-southeast-1_qDdEa4ZHy
Amazon Cognito Domain: https://cms-sit.auth.ap-southeast-1.amazoncognito.com/oauth2/token

[cms-cognito-sit-pns-client]
App Client ID: 32uncep9tke59v3on88qe6mbsk
App Client Secret: 1bp8mu5r1lursj6gtg393m80h4q0nievr7sbkjnaso301l9uu5v9

[cms-cognito-sit-hago-client]
App Client ID: 2d64vs1o644bl11b8d4kblh65p
App Client Secret: fii0sskanhgnrq3mnaiukvqpqo78quia7ipm4lk7okio6k2upha

```

## lpt
```
Save Notification API: https://funj8598fk.execute-api.ap-east-1.amazonaws.com/lpt/v1/notification/create
Retrieve Notification API: https://funj8598fk.execute-api.ap-east-1.amazonaws.com/lpt/v1/notification/retrieve

Pool Id: ap-southeast-1_uIFaaAqlF
Pool ARN: arn:aws:cognito-idp:ap-southeast-1:303498446923:userpool/ap-southeast-1_uIFaaAqlF
Amazon Cognito Domain: https://cms-lpt.auth.ap-southeast-1.amazoncognito.com/oauth2/token

[cms-cognito-lpt-pns-client]
App Client ID: 2a6aihruqnutimeekfidp1g3tu
App Client Secret: 7d0nisk054ff3n7c5qgd3t9a2fhmmem28uuf9qlm7co3j40rvim

[cms-cognito-lpt-hago-client]
App Client ID: 17sgdvks9h4udea18h9h4jgkes
App Client Secret: ilv06qfr1tvmn8e9ehmh5ugtsus4eutnk7upq1m9tg0ltrsnsg

```

## lpt (obsolete)
```
Save Notification API: https://0hl09vr6je.execute-api.ap-southeast-1.amazonaws.com/lpt/v1/notification/create
Amazon Cognito Domain: https://lpt-notification-api.auth.ap-southeast-1.amazoncognito.com
App Client ID: 1a4h3jjbtlsci7jpdi5btd3ae1
App Client Secret: iqn5u85unc488891vvppfdfkr68vp67nd1mcu9aqpm985ufferf

```

## tst (obsolete)
```
Save Notification API: https://41qf8kwqr0.execute-api.ap-southeast-1.amazonaws.com/tst/v1/notification/create
Amazon Cognito Domain: https://tst-notification-api.auth.ap-southeast-1.amazoncognito.com
App Client ID: 409vt1fjds2f3qc97jd0jfdr4s
App Client Secret: 1sld7b88bi7k6cbk000ge3s5kd3k1p921nu9d9pmuq9fj0nnfdpf

```

## dev (obsolete)
```

Save Notification API: https://1o44klvzjk.execute-api.ap-southeast-1.amazonaws.com/dev/v1/notification/create
Amazon Cognito Domain: https://dev-notification-api.auth.ap-southeast-1.amazoncognito.com
App Client ID: 1j58he95qemn4ecgfcn0nonk2g
App Client Secret: 20kesmosr5staho07665r6necq6ksciu8oqf4ehinace6376fs1


```


