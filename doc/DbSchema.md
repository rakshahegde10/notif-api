---
id: DbSchema
title: DB Schema
sidebar_label: DB Schema.
---

## (NoSQL) Database Schema

    {
        TableName: "Notification",
        Item :
        {
            "id" : "4b248b3c-c7f9-422b-9e8f-7a574cc4c685",
            "appUserTargetId": "hk.org.ha.hago:2019040913083656642538:B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3",
            "appTargetId": "hk.org.ha.hago:B9EF21EE-EA9B-4C9E-9D77-949A7A944AF3",
            "applicationId" : "hk.org.ha.hago",
            "subApplicationId" : "bookha",
            "targetId" : "cb5255f8-9778-4734-aeda-e9149f616071",
            "userId" : 201912120000199200",
            "alert" : "HA Go - Test Alert",
            "badge" : 1,
            "sound" : "default",
            "data" : "It is a testing content",
            "customSearchKey": "{\"login_required\":\"Y\"}",
            "createDate" : "2010-12-21T17:42:34+08:00",
            "version" : 1
        }
    }    
    
    {
        TableName: "SysParam",
        Item :
        {
            "id" : "dd848b3c-c7f9-422b-9e8f-7e574cc9c777",
            "applicationId" : "hk.org.ha.hago",
            "requesterId" : "bookha",
            "parameters" : { {'filter-retention' : 7 } },
            "active" : true,
            "createDate" : "2010-12-21T17:42:34+08:00",
            "updateDate" : "2010-12-21T17:42:34+08:00",
            "vesrion" : 1
        }
    }
