---
id: SourceCodeManagement 
title: Source Code Management
sidebar_label: Source Code Management 
---

## Best Practices for Using Git

1.	Make clean, single-purpose commits
2.	Write meaningful commit messages
3.	Commit early, commit often
4.	Don’t alter published history

Please refer to the follow website for details:
https://deepsource.io/blog/git-best-practices/

## GitLab Workflow

GitLab Flow is a simple alternative to more complex git flow. The following diagram is the GitLab flow involving a master and 2 feature branches.
master branch – the protected and long-lived main branch 
feature branch – the branch of the repository to work on a feature, it will be merged to master branch when the feature is done

![GitHub Logo](./img/GitLabFlow.png)

1.	Create 2 merge requests with feature-1 and feature-2 branches.
2.	When feature-2 is done, merge the feature 2 to master branch. The feature-2 branch can be deleted optionally.
3.	After the merge to master, the master branch contains feature-2. The feature-1 branch remains there for ongoing update.
4.	When feature-1 is done, it can be merged to master branch. If conflict exists, merge back master to feature-1 and developer resolves the conflict.
5.	Merge the feature 1 to master branch and the feature-1 branch can be deleted optionally.
6.	After the merge to master, the master branch contains both feature-2 and feature-1.
